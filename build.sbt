name := "gbash"

version := "1.0"

scalaVersion := "2.11.4"

libraryDependencies += "org.scalatest" % "scalatest_2.11" % "2.2.1" % "test"

libraryDependencies += "org.scala-lang" % "scala-swing" % "2.11+"

mainClass in assembly := Some("com.greasybubbles.gbash.Main")