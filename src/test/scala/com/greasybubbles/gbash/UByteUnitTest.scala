package com.greasybubbles.gbash

import com.greasybubbles.gbash.cpu.DmgCpu$
import com.greasybubbles.gbash.mem.DmgMem
import org.scalatest.{Matchers, FlatSpec}

class UByteUnitTest extends FlatSpec with Matchers {

  "The UByte type" should "handle half-carry properly when adding" in {
    val (_, _, halfCarry) = new UByte(0x18) adc new UByte(0x09)
    halfCarry should be (true)

    val (_, _, halfCarry2) = new UByte(0x18) adc new UByte(0x07)
    halfCarry2 should be (false)
  }

  it should "handle carry properly when adding" in {
    val (_, carry, _) = new UByte(0x47) adc new UByte(0x2c)
    carry should be (false)

    val (_, carry2, _) = new UByte(0x49) adc new UByte(0xe6)
    carry2 should be (true)
  }

  it should "handle half-carry properly when subtracting" in {
    val (_, _, halfCarry) = new UByte(0x15) sbc new UByte(0x21)
    halfCarry should be (false)

    val (_, _, halfCarry2) = new UByte(0x17) sbc new UByte(0x08)
    halfCarry2 should be (true)
  }

  it should "handle carry properly when subtracting" in {
    val (_, carry, _) = new UByte(0x47) sbc new UByte(0x28)
    carry should be (false)

    val (_, carry2, _) = new UByte(0x1c) sbc new UByte(0x2b)
    carry2 should be (true)
  }

}
