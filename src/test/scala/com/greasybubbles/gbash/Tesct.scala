package com.greasybubbles.gbash

import com.greasybubbles.gbash.cpu.{DmgCpu, B, A, DmgCpu$}
import com.greasybubbles.gbash.input.DmgInput
import com.greasybubbles.gbash.mem.DmgMem

import collection.mutable.Stack
import org.scalatest._

class ADDTest extends FlatSpec with Matchers {

  val testCpuState = new DmgCpu(0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x1234, 0x5678, false, false)
  def testState = new Dmg(null, DmgMem.Initial, DmgCpu.Initial, DmgInput.Neutral, cycleNr = 0)

  "The ADD instruction" should "compute the correct sum when not overflowing" in {
    val state = testState.poke[UByte](A, 0x3e).poke[UByte](B, 0x13)
    val instruction = ADD(B)
    val (_, newState) = instruction.execute.run(state)
    newState.cpu.read(A) should be(new UByte(0x51))
    newState.cpu.carry should be (false)
    newState.cpu.halfCarry should be (true)
  }

  it should "compute the correct sum and set the carry flag when overflowing" in {
    val state = testState.poke[UByte](A, 0xf0).poke[UByte](B, 0x12)
    val instruction = ADD(B)
    val (_, newState) = instruction.execute.run(state)
    newState.cpu.read(A) should be(new UByte(0x02))
    newState.cpu.carry should be (true)
    newState.cpu.halfCarry should be (false)
  }

  it should "set the zero flag when the result is zero" in {
    val state = testState.poke[UByte](A, 0xf8).poke[UByte](B, 0x08)
    val instruction = ADD(B)
    val (_, newState) = instruction.execute.run(state)
    newState.cpu.zero should be (true)
  }

  it should "reset the subtract flag" in {
    val state = testState.poke[UByte](A, 0x04).poke[UByte](B, 0x05)
    val instruction = ADD(B)
    val (_, newState) = instruction.execute.run(state)
    newState.cpu.subtract should be (false)
  }
}


class DAATest extends FlatSpec with Matchers {

  val testCpuState = new DmgCpu(0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x1234, 0x5678, false, false)
  def testState = new Dmg(null, DmgMem.Initial, DmgCpu.Initial, DmgInput.Neutral, cycleNr = 0)

  "The DAA instruction" should "answer the following BCD operation correctly: 35-12 = 23" in {
    val state = testState.poke[UByte](A, 0x35).poke[UByte](B, 0x12)
    val (_, newState) = (SUB(B).execute andThen DAA.execute).run(state)
    newState.cpu.read(A) should be(new UByte(0x23))
    newState.cpu.carry should be (false)
    newState.cpu.halfCarry should be (false)
  }

  it should "answer the following BCD operation correctly: 35-16 = 19" in {
    val state = testState.poke[UByte](A, 0x35).poke[UByte](B, 0x16)
    val (_, newState) = (SUB(B).execute andThen DAA.execute).run(state)
    //newState.cpu.read(A) should be(new UByte(0x19))
    newState.cpu.carry should be (false)
    newState.cpu.halfCarry should be (true)
  }

  it should "answer the following BCD operation correctly: 19-21 = 98" in {
    val state = testState.poke[UByte](A, 0x19).poke[UByte](B, 0x21)
    val (_, newState) = (SUB(B).execute andThen DAA.execute).run(state)
    newState.cpu.read(A) should be(new UByte(0x98))
    newState.cpu.carry should be (true)
    newState.cpu.halfCarry should be (false)
  }

  it should "answer the following BCD operation correctly: 15-38 = 77" in {
    val state = testState.poke[UByte](A, 0x15).poke[UByte](B, 0x38)
    val (_, newState) = (SUB(B).execute andThen DAA.execute).run(state)
    //newState.cpu.read(A) should be(new UByte(0x77))
    newState.cpu.carry should be (true)
    newState.cpu.halfCarry should be (true)
  }
}