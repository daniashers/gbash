package com.greasybubbles.gbash

import com.greasybubbles.gbash.cpu.DmgCpu$
import com.greasybubbles.gbash.mem.DmgMem
import com.greasybubbles.gbash.parsers.{TableBasedParser, DmgAllInstructionParsers}

object Disasm {
  def main(args: Array[String]): Unit = {
    if (args.size != 3) {
      println("Usage: disasm file addr_start addr_end")
      ()
    }

    val filename = args(0)
    val startAddr = Integer.parseInt(args(1), 16)
    val endAddr = Integer.parseInt(args(2), 16)

    val cart = Cartridge.fromFile(filename)

    var addr = startAddr
    while (addr <= endAddr) {
      val instrStream: Stream[UByte] = Stream.from(addr).map(v => cart.read(new U16(v)))
      val instr = DmgAllInstructionParsers.anyParser.parse(instrStream)
      instr match {
        case None => {
          print(new U16(addr) + " " + "???")
          addr = addr + 1
        }
        case Some(i) => {
          print(new U16(addr) + " " + i)
          addr = addr + i.length
        }
      }
      println()
    }
  }
}