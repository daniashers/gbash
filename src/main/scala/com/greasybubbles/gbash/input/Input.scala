package com.greasybubbles.gbash.input


case class DmgInput(up: Boolean, down: Boolean, left: Boolean, right: Boolean,
                    a: Boolean, b: Boolean, select: Boolean, start: Boolean)

case object DmgInput {
  val Neutral = DmgInput(false,false,false,false,false,false,false,false)
}

case object KeyBindings {
  val Up = 87
  val Down = 83
  val Left = 65
  val Right = 68
  val A = 76
  val B = 75
  val Start = 66
  val Select = 86
  //TODO others
}