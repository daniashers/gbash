package com.greasybubbles.gbash

trait Lens[T,V] {
  def get(t: T): V
  def set(t: T, v: V): T
}

object Lens {
  def make[T,V](f: T => V, g: (T,V) => T) = new Lens[T,V] {
    override def get(t: T): V = f(t)
    override def set(t: T, v: V): T = g(t,v)
  }
  def const[T,V](value: V) = new Lens[T,V] {
    override def get(t: T): V = value
    override def set(t: T, v: V): T = t
  }
  def compose[T,U,V](outer: Lens[T,U], inner: Lens[U,V]): Lens[T,V] = new Lens[T,V] {
    def get(t: T): V = inner.get(outer.get(t))
    def set(t: T, v: V): T = outer.set(t, inner.set(outer.get(t), v))
  }
}

class LoggingLens[T,V](orig: Lens[T,V], label: String) extends Lens[T,V] {
  override def get(t: T): V = {
    println("Reading " + label)
    orig.get(t)
  }
  override def set(t: T, v: V): T = {
    println("Writing " + label)
    orig.set(t,v)
  }
}

trait ParamLens[T,V,P] {
  def get(param: P)(t: T): V
  def set(param: P)(t: T, v: V): T
}