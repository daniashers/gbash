package com.greasybubbles.gbash.mem

import com.greasybubbles.gbash.{U16, UByte}

trait Mem {
  def read(addr: Int): UByte
  def write(addr: Int, v: UByte): Mem
}

/**
 * A 'dummy' memory representation to stand for unaddressed space
 * that should return zero when read and not register any writes.
 */
object UnaddressedSpace extends Mem {
  override def write(addr: Int, v: UByte): Mem = this
  override def read(addr: Int): UByte = UByte.ZERO
}

// THIS CLASS IS MUTABLE AND MUST ONLY BE USED IN SAFE WAYS
class MutableRam(val size: Int, val baseAddr: Int = 0) extends Mem { //extends RWable[MutableRam, U16, UByte] {
  var data: Array[Byte] = Array.fill(size)(0)
  def read(addr: Int): UByte = UByte.fromSigned(data(addr - baseAddr))
  def write(addr: Int, value: UByte): MutableRam = {
    data(addr - baseAddr) = value.signedValue
    this
  }
}

class ImmutableRam(data: Vector[UByte]) extends Mem {
  val size = data.size
  def read(addr: Int): UByte = data(addr)
  def write(addr: Int, value: UByte): ImmutableRam = new ImmutableRam(data.updated(addr, value))
}

object ImmutableRam {
  def apply(size: Int) = new ImmutableRam(Vector.fill(size)(0))
}

class Rom(dataz: IndexedSeq[Byte]) extends Mem {
  private val data: Array[UByte] = Array.tabulate(dataz.size)(i => new UByte(dataz(i)))
  def read(addr: Int): UByte = data(addr)
  def write(addr: Int, value: UByte): Rom = this // ROM can't be written to, return unchanged self
}

class MemoryRegion(origin: Mem, val offset: Int, val length: Int) {
  def apply(addr: Int): UByte = {
    if (addr >= 0 && addr < length) origin.read(offset + addr)
    else throw new RuntimeException("Index out of bounds - requested: " + addr + ", length: " + length)
  }
  def subregion(suboffset: Int, sublength: Int): MemoryRegion = new MemoryRegion(origin, offset+suboffset, sublength)
  def subregionOrg(newoffset: Int, newlength: Int): MemoryRegion = new MemoryRegion(origin, newoffset, newlength)
  //TODO the subregion function above uses "original" offsets and not sub-offset for the moment. Is this the best api design?
  def dump: String = {
    def ValuesPerLine = 16
    def sequence(first: Int, howMany: Int): String = {
      List.tabulate(howMany)(i => this(first+i)).foldLeft("")((acc,v) => acc + "%02X ".format(v.value))
    }
    def line(ln: Int): String = sequence(ValuesPerLine * ln, ValuesPerLine)
    val nrOfWholeLines: Int = length / ValuesPerLine
    val remainder = length % ValuesPerLine
    "MemoryRegion base address %x length %x\n".format(offset,length) +
      (0 to nrOfWholeLines-1).foldLeft("")((acc,v) => acc + line(v) + "\n") +
      sequence(length-remainder, remainder)
  }
}
