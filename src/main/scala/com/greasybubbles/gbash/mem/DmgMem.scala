package com.greasybubbles.gbash.mem

import com.greasybubbles.gbash._
import com.greasybubbles.gbash.cpu.{Imm16, Ind8}
import com.greasybubbles.gbash.graphics.VideoData

import scala.annotation.tailrec

case class DmgMem(videoRam: Mem, internalRam: Mem, oam: Mem, ioRegisters: Mem, highRam: Mem) extends Mem {

  def lens(address: U16): Lens[DmgMem,UByte] =
    Lens.compose[DmgMem,Mem,UByte](DmgMem.subChipLens(address.value), Lens.make[Mem,UByte](_.read(address.value),(t,v)=>t.write(address.value,v)))

  def lensor(address: U16): ParamLens[DmgMem,UByte,U16] = {
    val subchipLens = DmgMem.subChipLens(address.value)
    new ParamLens[DmgMem,UByte,U16] {
      override def get(param: U16)(t: DmgMem): UByte = subchipLens.get(t).read(param.value)
      override def set(param: U16)(t: DmgMem, v: UByte): DmgMem = subchipLens.set(t, subchipLens.get(t).write(param.value,v))
    }
  }

  def videoData: VideoData = new VideoData(
    new MemoryRegion(videoRam, 0x8000, 0x2000),
    new MemoryRegion(oam, 0xfe00, 0xa0),
    new MemoryRegion(ioRegisters, 0xff40, 0xc))
  def registers = new MemoryRegion(ioRegisters, 0xFF00, 0x50)

  override def read(addr: Int): UByte = DmgMem.subChipLens(addr).get(this).read(addr)
  override def write(addr: Int, v: UByte): Mem = {
    val sublens = DmgMem.subChipLens(addr)
    sublens.set(this, sublens.get(this).write(addr, v))
  }
}

object DmgMem {

  def subChipLens(address: Int): Lens[DmgMem,Mem] = {
    val a = address
    if      (0x8000 <= a && a <= 0x9FFF) DmgMem.videoRamLens
    else if (0xC000 <= a && a <= 0xDFFF) DmgMem.internalRamLens
    else if (0xE000 <= a && a <= 0xFDFF) DmgMem.internalRamLens //echo of internal RAM TODO! but remove address MSB or out of bounds
    else if (0xFE00 <= a && a <= 0xFE9F) DmgMem.oamLens
    else if (0xFF00 <= a && a <= 0xFF50) DmgMem.ioRegistersLens
    else if (0xFF80 <= a && a <= 0xFFFF) DmgMem.highRamLens
    else Lens.const[DmgMem,Mem](UnaddressedSpace) //TODO!
  }

  val videoRamLens = new Lens[DmgMem,Mem] {
    override def get(t: DmgMem): Mem = t.videoRam
    override def set(t: DmgMem, v: Mem): DmgMem = t.copy(videoRam = v)
  }
  val internalRamLens = new Lens[DmgMem,Mem] {
    override def get(t: DmgMem): Mem = t.internalRam
    override def set(t: DmgMem, v: Mem): DmgMem = t.copy(internalRam = v)
  }
  val oamLens = new Lens[DmgMem,Mem] {
    override def get(t: DmgMem): Mem = t.oam
    override def set(t: DmgMem, v: Mem): DmgMem = t.copy(oam = v)
  }
  val ioRegistersLens = new Lens[DmgMem,Mem] {
    override def get(t: DmgMem): Mem = t.ioRegisters
    override def set(t: DmgMem, v: Mem): DmgMem = t.copy(ioRegisters = v)
  }
  val highRamLens = new Lens[DmgMem,Mem] {
    override def get(t: DmgMem): Mem = t.highRam
    override def set(t: DmgMem, v: Mem): DmgMem = t.copy(highRam = v)
  }

  val Initial = DmgMem(
    videoRam    = new MutableRam(8192, baseAddr = 0x8000),  // 8000-9FFF (8k)
    internalRam = new MutableRam(8192, baseAddr = 0xC000),  // C000-DFFF (8k)
    oam         = new MutableRam( 160, baseAddr = 0xFE00),  // FE00-FE9F (160 bytes)
    ioRegisters = new MutableRam( 128, baseAddr = 0xFF00),  // FF00-FF4C
    highRam     = new MutableRam( 128, baseAddr = 0xFF80)   // FF80-FFFF (128 bytes)
  )
}

//FF00 (P1) Register for reading joy pad info and determining system type.
//FF01 (SB) Serial transfer data (R/W)
//FF02 (SC) SIO control (R/W)
//FF04 (DIV) Divider register (R/W)
//FF05 (TIMA) Timer counter (R/W)
//FF06 (TMA) Timer Modulo (R/W)
//FF07 (TAC) Timer Control (R/W)
//FF0F (IF) Interrupt Flag (R/W)

//FF10 (NR10) Sound Sweep Mode 1 register (R/W)
//FF11 (NR11) Sound Mode 1 register, Sound length/Wave pattern duty (R/W)
//FF12 (NR12) Sound Mode 1 register, Envelope (R/W)
//FF13 (NR13) Sound Mode 1 register, Frequency lo (W)
//FF14 (NR14) Sound Mode 1 register, Frequency hi (R/W)

//FF16 (NR21) Sound Mode 2 register, Sound Length; Wave Pattern Duty (R/W)
//FF17 (NR22) Sound Mode 2 register, envelope (R/W)
//FF18 (NR23) Sound Mode 2 register, frequency lo data (W)
//FF19 (NR24) Sound Mode 2 register, frequency hi data (R/W)

//FF1A (NR30) Sound Mode 3 register, Sound on/off (R/W) Only bit 7 can be read
//FF1B (NR31) Sound Mode 3 register, sound length (R/W)
//FF1C (NR32) Sound Mode 3 register, Select output
//FF1D (NR33) Sound Mode 3 register, frequency's lower data (W)
//FF1E (NR34) Sound Mode 3 register, frequency's higher data (R/W) Only bit 6 can be read

//FF20 (NR41) Sound Mode 4 register, sound length (R/W)
//FF21 (NR42) Sound Mode 4 register, envelope (R/W)
//FF22 (NR43) Sound Mode 4 register, polynomial counter (R/W)
//FF23 (NR44) Sound Mode 4 register

//FF24 (NR50) Channel control / ON-OFF / Volume (R/W)
//FF25 (NR51) Selection of Sound output terminal (R/W)
//FF26 (NR52) Sound on/off (R/W)
//FF30 - FF3F (Wave Pattern RAM)

//FF40 (LCDC) LCD Control (R/W)
//FF41 (STAT) LCDC Status (R/W)
//FF42 (SCY) Scroll Y (R/W)
//FF43 (SCX) Scroll X (R/W)
//FF44 (LY) LCDC Y-Coordinate (R)
//FF45 (LYC) LY Compare (R/W)
//FF46 (DMA) DMA Transfer and Start Address (W)
//FF47 (BGP) BG & Window Palette Data (R/W)
//FF48 (OBP0) Object Palette 0 Data (R/W)
//FF49 (OBP1) Object Palette 1 Data (R/W)
//FF4A (WY) Window Y Position (R/W)
//FF4B (WX) Window X Position (R/W)