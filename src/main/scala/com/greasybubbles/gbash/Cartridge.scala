package com.greasybubbles.gbash

import java.io.{File, FileInputStream}

import com.greasybubbles.gbash.mem._

// This class is still very messy and hacked together.

case class Cartridge(val romBanks: Vector[Rom], ram: Mem = UnaddressedSpace, romBankNr: Int = 1, ramBankNr: Int = 0) {
  import Cartridge._
  def read(address: U16): UByte = {
    if (address.value < 0x8000) { //this is a read from ROM
      val romBankToReadFrom = if (address.value < RomBankSize) 0 else romBankNr //0x0000 to 0x3fff is always bank 0
      romBanks(romBankToReadFrom).read(address.value % RomBankSize)
    } else if (0xa000 <= address.value && address.value < 0xc000) { //this is a read from RAM
      val internalAddress = ramBankNr * RamBankSize + (address.value - 0xa000)
      ram.read(internalAddress)
    } else UByte.ZERO
  }
  def write(address: U16, value: UByte): Cartridge = {
    if (address.value >= 0x2000 && address.value <= 0x3fff) { //this is a ROM bank switch command
      this.copy(romBankNr = (value.value & 0x1f).max(1)) //max(1) because you can't select bank zero,if you do you get bank 1
    } else if (0x4000 <= address.value && address.value <= 0x5fff) { //this is a RAM bank switch command
      println("Switching to RAM bank " + value)
      this.copy(ramBankNr = (value.value & 0x03))
      //this.copy(ramBankNr = value.value & 0x0f) //TODO! The max number of RAM banks is determined by MBC type. Don't hardcode!
    } else if (0xa000 <= address.value && address.value < 0xc000) { //this is a write to RAM
      val internalAddress = ramBankNr * RamBankSize + (address.value - 0xa000)
      this.copy(ram = ram.write(internalAddress, value))
    }
    else this
  }
}

object Cartridge {

  val RomBankSize = 16*1024 // 16k
  val RamBankSize =  8*1024 //  8k

  def lens(address: U16): Lens[Cartridge,UByte] =
    Lens.make[Cartridge,UByte](_.read(address), (t,v) => t.write(address, v))

  def fromFile(filename: String): Cartridge = fromFile(new File(filename))

  def fromFile(file: File): Cartridge = {

    val fileSize = file.length
    val nrOfBanksOnFile = (fileSize / RomBankSize).toInt

    val filestream = new FileInputStream(file)

    val cartInfo = new Array[Byte](0x50)
    filestream.skip(0x100)
    filestream.read(cartInfo, 0, 0x50) //reads cartridge info area

    val title: String = (0x34 to 0x42).map(cartInfo(_).toChar).foldLeft("")(_ + _)
    println("Cart title: " + title)

    val romSize = MBC.romSize(cartInfo(0x48))
    val ramSize = MBC.ramSize(cartInfo(0x49))
    val cartTypeCode = cartInfo(0x47)

    println("Cart type code: " + cartTypeCode)
    println("ROM size: %d Kbit (%d KB) (%d banks)".format(romSize*8/1024, romSize/1024, romSize/RomBankSize))
    println("RAM size: %d KB".format(ramSize/1024))

    val flatData = new Array[Byte](romSize)
    new FileInputStream(file).read(flatData) //reads everything

    def extractBank(data: Array[Byte], bankNr: Int): Rom = {
      new Rom(Array.tabulate[Byte](16384)(i => data(16384*bankNr + i)))
    }

    val romBanx: Vector[Rom] = Vector.tabulate(nrOfBanksOnFile)(extractBank(flatData, _))

    if (ramSize != 0) new Cartridge(romBanx, new MutableRam(ramSize,0))//ImmutableRam(ramSize))
    else new Cartridge(romBanx)
  }
}

case class CartInfo()

object MBC {
  //  0-ROM ONLY
  //  1-MBC1
  //  2-MBC1+RAM
  //  3-MBC1+RAM+BATT
  //
  //  5-MBC2
  //  6-MBC2+BATTERY
  //
  //  8-RAM
  //  9-RAM+BATTERY
  //
  //  B-MMM01
  //  C-MMM01+SRAM
  //  D-MMM01+SRAM+BATT
  //  F-MBC3+TIMER+BATT
  // 10-MBC3+TIMER+RAM+BATT
  // 11-MBC3
  // 12-MBC3+RAM
  // 13-MBC3+RAM+BATT
  //
  //
  //
  //
  //
  // 19-MBC5
  // 1A-MBC5+RAM
  // 1B-MBC5+RAM+BATT
  // 1C-MBC5+RUMBLE
  // 1D-MBC5+RUMBLE+SRAM
  // 1E-MBC5+RUMBLE+SRAM+BATT

  def romBanksNr(code: Int): Int = code match {
    case 0 => 2
    case 1 => 4
    case 2 => 8
    case 3 => 16
    case 4 => 32
    case 5 => 64
    case 6 => 128
    case 0x52 => 72
    case 0x53 => 80
    case 0x54 => 96
  }

  def romSize(code: Int) = romBanksNr(code) * Cartridge.RomBankSize

  val ramSize = Vector[Int] (
    0,
    2 * 1024,
    8 * 1024,
    32 * 1024,
    128 * 1024
  )

  val ramBankSize = 8 * 1024

  def ramBanks(i: Int) = ramSize(i) / ramBankSize
}