package com.greasybubbles.gbash

object MemoryMapConstants {
  val P1   = 0xff00   // Register for reading joy pad info and determining system type.
  val SB   = 0xff01   // Serial transfer data (R/W)
  val SC   = 0xff02   // SIO control (R/W)
  val DIV  = 0xff04   // Divider register (R/W)
  val TIMA = 0xff05   // Timer counter (R/W)
  val TMA  = 0xff06   // Timer Modulo (R/W)
  val TAC  = 0xff07   // Timer Control (R/W)
  val IF   = 0xff0f   // Interrupt Flag (R/W)
  //  FF10 (NR10) Sound Sweep Mode 1 register (R/W)
  //  FF11 (NR11) Sound Mode 1 register, Sound length/Wave pattern duty (R/W)
  //  FF12 (NR12) Sound Mode 1 register, Envelope (R/W)
  //  FF13 (NR13) Sound Mode 1 register, Frequency lo (W)
  //  FF14 (NR14) Sound Mode 1 register, Frequency hi (R/W)
  //  FF16 (NR21) Sound Mode 2 register, Sound Length; Wave Pattern Duty (R/W)
  //  FF17 (NR22) Sound Mode 2 register, envelope (R/W)
  //  FF18 (NR23) Sound Mode 2 register, frequency lo data (W)
  //  FF19 (NR 24) Sound Mode 2 register, frequency hi data (R/W)
  //  FF1A (NR30)
  //  FF1B (NR31) Sound Mode 3 register, sound length (R/W)
  //  FF1C (NR32) Sound Mode 3 register, Select output
  //  FF1D (NR33) Sound Mode 3 register, frequency's lower data (W)
  //  FF1E
  //  FF20 (NR41)
  //  FF21 (NR42) Sound Mode 4 register, envelope (R/W)
  //  FF22 (NR43) Sound Mode 4 register, polynomial counter (R/W)
  //  FF23 (NR44) Sound Mode 4 register
  //  FF24 (NR50) Channel control / ON-OFF / Volume (R/W)
  //  FF25 (NR51) Selection of Sound output terminal (R/W)
  //  FF26 (NR52) Sound on/off (R/W)
  //  FF30 - FF3F (Wave Pattern RAM)

  val LCDC = 0xff40   // LCD Control (R/W)
  val STAT = 0xff41   // LCDC Status (R/W)
  val SCY  = 0xff42   // Scroll Y (R/W)
  val SCX  = 0xff43   // Scroll X (R/W)
  val LY   = 0xff44   // LCDC Y-Coordinate (R)
  val LYC  = 0xff45   // LY Compare (R/W)
  val DMA  = 0xff46   // DMA Transfer and Start Address (W)
  val BGP  = 0xff47   // BG & Window Palette Data (R/W)
  val OBP0 = 0xff48   // Object Palette 0 Data (R/W)
  val OBP1 = 0xff49   // Object Palette 1 Data (R/W)
  val BOOT = 0xff50   // Bootrom enable
  val WY   = 0xff4a   // Window Y Position (R/W)
  val WX   = 0xff4b   // Window X Position (R/W)
  val IE   = 0xffff   // Interrupt Enable
}