package com.greasybubbles.gbash.cpu

import com.greasybubbles.gbash._

case class DmgCpu(val a: UByte, val b: UByte, val c: UByte, val d: UByte,
                    val e: UByte, val f: UByte, val h: UByte, val l: UByte,
                    val sp: U16, // stack pointer
                    val pc: U16, // program counter
                    val ime: Boolean, // interrupt master enable
                    val halted: Boolean) {

  def af: U16 = a hl f
  def bc: U16 = b hl c
  def de: U16 = d hl e
  def hl: U16 = h hl l
  
  def a(v: UByte) = DmgCpu(v, b, c, d, e, f, h, l, sp, pc, ime, halted)
  
  def af(v: U16) = DmgCpu(v.hi, b, c, d, e, v.lo, h, l, sp, pc, ime, halted)
  def bc(v: U16) = DmgCpu(a, v.hi, v.lo, d, e, f, h, l, sp, pc, ime, halted)
  def de(v: U16) = DmgCpu(a, b, c, v.hi, v.lo, f, h, l, sp, pc, ime, halted)
  def hl(v: U16) = DmgCpu(a, b, c, d, e, f, v.hi, v.lo, sp, pc, ime, halted)
  def sp(v: U16): DmgCpu = DmgCpu(a, b, c, d, e, f, h, l, v, pc, ime, halted)
  def pc(v: U16): DmgCpu = DmgCpu(a, b, c, d, e, f, h, l, sp, v, ime, halted)
  def ime(v: Boolean): DmgCpu = DmgCpu(a, b, c, d, e, f, h, l, sp, pc, v, halted)

  def read(r: DmgRegister8): UByte = {
    //r.get(this)
    r match {
      case A => a
      case B => b
      case C => c
      case D => d
      case E => e
      case F => f
      case H => h
      case L => l
    }
  }

  def read16(r: DmgRegister16): U16 = {
//    r.get(this)
    r match {
      case AF => af
      case BC => bc
      case DE => de
      case HL => hl
      case SP => sp
      case PC => pc
    }
  }

  def write(r: DmgRegister8, v: UByte): DmgCpu = r match {
    case A => DmgCpu(v, b, c, d, e, f, h, l, sp, pc, ime, halted)
    case B => DmgCpu(a, v, c, d, e, f, h, l, sp, pc, ime, halted)
    case C => DmgCpu(a, b, v, d, e, f, h, l, sp, pc, ime, halted)
    case D => DmgCpu(a, b, c, v, e, f, h, l, sp, pc, ime, halted)
    case E => DmgCpu(a, b, c, d, v, f, h, l, sp, pc, ime, halted)
    case F => DmgCpu(a, b, c, d, e, v, h, l, sp, pc, ime, halted)
    case H => DmgCpu(a, b, c, d, e, f, v, l, sp, pc, ime, halted)
    case L => DmgCpu(a, b, c, d, e, f, h, v, sp, pc, ime, halted)
  }

  def write16(r: DmgRegister16, v: U16): DmgCpu = r match {
    case AF => this.af(v)
    case BC => this.bc(v)
    case DE => this.de(v)
    case HL => this.hl(v)
    case SP => this.sp(v)
    case PC => this.pc(v)
    case _ => this
  }

  def readFlag(f: DmgRegisterFlag): Boolean = f match {
    case IME       => ime
    case Zero      => zero
    case Subtract  => subtract
    case Carry     => carry
    case HalfCarry => halfCarry
  }

  def writeFlag(f: DmgRegisterFlag, v: Boolean): DmgCpu = f match {
    case IME       => ime(v)
    case Zero      => zero(v)
    case Subtract  => subtract(v)
    case Carry     => carry(v)
    case HalfCarry => halfCarry(v)
  }

  def zero(state: Boolean): DmgCpu = this.write(F, if (state) f.set(7) else f.reset(7))
  def subtract(state: Boolean): DmgCpu = this.write(F, if (state) f.set(6) else f.reset(6))
  def halfCarry(state: Boolean): DmgCpu = this.write(F, if (state) f.set(5) else f.reset(5))
  def carry(state: Boolean): DmgCpu = this.write(F, if (state) f.set(4) else f.reset(4))

  def zero = f.test(7)
  def subtract = f.test(6)
  def halfCarry = f.test(5)
  def carry = f.test(4)

  def halt = this.copy(halted = true)

  override def toString = {
    def btos(v: Boolean, t: String, f: String): String = if (v) t else f
    val flags = btos(zero, "Z", "-") + btos(subtract, "N", "-") + btos(carry, "C", "-") + btos(halfCarry, "H", "-")
    "Dmg CPU: PC"+pc+" A"+a+" BC"+bc+" DE"+de+" HL"+hl+" SP"+sp+" "+flags+
      " IME "+btos(ime,"on ","off")+" halted="+btos(halted,"yes","no ")
  }
}

object DmgCpu {
  def lens(r: DmgRegister8): Lens[DmgCpu,UByte] = new Lens[DmgCpu, UByte] {
    override def get(t: DmgCpu): UByte = t.read(r)
    override def set(t: DmgCpu, v: UByte): DmgCpu = t.write(r, v)
  }
  def lens(r: DmgRegister16): Lens[DmgCpu,U16] = new Lens[DmgCpu, U16] {
    override def get(t: DmgCpu): U16 = t.read16(r)
    override def set(t: DmgCpu, v: U16): DmgCpu = t.write16(r, v)
  }
  def lens(f: DmgRegisterFlag): Lens[DmgCpu,Boolean] = new Lens[DmgCpu, Boolean] {
    override def get(t: DmgCpu): Boolean = t.readFlag(f)
    override def set(t: DmgCpu, v: Boolean): DmgCpu = t.writeFlag(f, v)
  }

  val Initial = DmgCpu(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, false, false)
}

class Flags(val f: UByte) {
  def Z: Boolean = f.test(7)
  def N: Boolean = f.test(6)
  def H: Boolean = f.test(5)
  def C: Boolean = f.test(4)
}