package com.greasybubbles.gbash.cpu

import com.greasybubbles.gbash.{Operand, U16, Dmg, UByte}

trait DmgOperand[A] extends Operand[Dmg,A]

sealed trait DmgRegister[A] extends DmgOperand[A] {
  def get(cpu: DmgCpu): A
}

sealed trait DmgRegister8 extends DmgRegister[UByte]
sealed trait DmgRegister16 extends DmgRegister[U16]
sealed trait DmgRegisterFlag extends DmgRegister[Boolean]

case object A extends DmgRegister8 { override def get(cpu: DmgCpu): UByte = cpu.a }
case object B extends DmgRegister8 { override def get(cpu: DmgCpu): UByte = cpu.b }
case object C extends DmgRegister8 { override def get(cpu: DmgCpu): UByte = cpu.c }
case object D extends DmgRegister8 { override def get(cpu: DmgCpu): UByte = cpu.d }
case object E extends DmgRegister8 { override def get(cpu: DmgCpu): UByte = cpu.e }
case object F extends DmgRegister8 { override def get(cpu: DmgCpu): UByte = cpu.f }
case object H extends DmgRegister8 { override def get(cpu: DmgCpu): UByte = cpu.h }
case object L extends DmgRegister8 { override def get(cpu: DmgCpu): UByte = cpu.l }
case object HL extends DmgRegister16 { override def get(cpu: DmgCpu): U16 = cpu.hl }
case object AF extends DmgRegister16 { override def get(cpu: DmgCpu): U16 = cpu.af }
case object BC extends DmgRegister16 { override def get(cpu: DmgCpu): U16 = cpu.bc }
case object DE extends DmgRegister16 { override def get(cpu: DmgCpu): U16 = cpu.de }
case object SP extends DmgRegister16 { override def get(cpu: DmgCpu): U16 = cpu.sp }
case object PC extends DmgRegister16 { override def get(cpu: DmgCpu): U16 = cpu.pc }

case object IME       extends DmgRegisterFlag { override def get(cpu: DmgCpu): Boolean = cpu.ime }
case object Zero      extends DmgRegisterFlag { override def get(cpu: DmgCpu): Boolean = cpu.zero }
case object Subtract  extends DmgRegisterFlag { override def get(cpu: DmgCpu): Boolean = cpu.subtract }
case object Carry     extends DmgRegisterFlag { override def get(cpu: DmgCpu): Boolean = cpu.carry }
case object HalfCarry extends DmgRegisterFlag { override def get(cpu: DmgCpu): Boolean = cpu.halfCarry }

trait Imm[A] extends DmgOperand[A] //immediate

case class Imm8(v: UByte) extends Imm[UByte] {override def toString = "#" + v.toString}
case class Imm16(v: U16) extends Imm[U16] {override def toString = "#" + v.toString}

trait Ind[A] extends DmgOperand[A] //indirect

case class Ind8(addr: DmgOperand[U16]) extends Ind[UByte] { override def toString = "("+addr+")" }
case class Ind16(addr: DmgOperand[U16]) extends Ind[U16] { override def toString = "("+addr+")" }

case class IndirectHi(addrIn: DmgOperand[UByte]) extends DmgOperand[UByte] { override def toString = "($FF00+"+addrIn+")" }

case class BitNumber(bitNr: Int) extends DmgOperand[Int]

sealed trait Condition {
  def check(cpu: DmgCpu): Boolean
}
case object Unconditional extends Condition { override def check(cpu: DmgCpu): Boolean = true }
case object Z extends Condition { override def check(cpu: DmgCpu): Boolean = cpu.zero }
case object NZ extends Condition { override def check(cpu: DmgCpu): Boolean = !cpu.zero }
case object CS extends Condition { override def check(cpu: DmgCpu): Boolean = cpu.carry }
case object NC extends Condition { override def check(cpu: DmgCpu): Boolean = !cpu.carry }
