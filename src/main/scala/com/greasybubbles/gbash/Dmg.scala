package com.greasybubbles.gbash

import com.greasybubbles.gbash.cpu._
import com.greasybubbles.gbash.input.DmgInput
import com.greasybubbles.gbash.mem.{Rom, DmgMem}
import com.greasybubbles.gbash.parsers._

import scala.annotation.tailrec

trait DmgInterruptGenerator

case class Dmg(cart: Cartridge, mem: DmgMem, cpu: DmgCpu, input: DmgInput, cycleNr: Long) {
  import MemoryMapConstants._

  def frame = cycleNr / Dmg.CyclesPerFrame
  def line: Int = ((cycleNr / Dmg.CyclesPerLine) % Dmg.LinesPerFrame).toInt
  def vblank: Boolean = (line >= 144)
  lazy val lcdcStatus: LCDCStatus = if (vblank) VBlankStatus else {
    val lineCycleNr = this.cycleNr % Dmg.CyclesPerLine
    /**/ if (lineCycleNr <  80) OamAccess
    else if (lineCycleNr < 252) VramAccess
    else HBlankStatus
  }

  def isTimerOn = peek(TAC).test(2)


  private def lens[A](op: DmgOperand[A]): Lens[Dmg,A] = (op match {
    case Ind8(Imm16(addr)) => {
      if (addr.value < 0x100 && bootRomEnabled) Lens.const[Dmg,UByte](Dmg.bootRom.read(addr.value))
      else if (addr.value >= 0x0000 && addr.value <= 0x7FFF) Lens.compose(Dmg.cartLens, Cartridge.lens(addr)) //cart ROM space
      else if (addr.value >= 0xA000 && addr.value <= 0xBFFF) Lens.compose(Dmg.cartLens, Cartridge.lens(addr)) //cart RAM space
      else if (addr.value == P1) new Lens[Dmg,UByte] { //TODO this logic should certainly live somewhere else. The mem class?
        val normalLens = Lens.compose(Dmg.memLens, mem.lens(addr))
        //Button reading matrix
        override def get(t: Dmg): UByte = {
          val currentValue = normalLens.get(t)
          if (currentValue.test(4)) {
            currentValue.bit(0, !input.a).bit(1, !input.b).bit(2, !input.select).bit(3, !input.start)
          } else if (currentValue.test(5)) {
            currentValue.bit(0, !input.right).bit(1, !input.left).bit(2, !input.up).bit(3, !input.down)
          } //TODO what if both bits 4 and 5 are set?
          else (currentValue | 0x0f)
        }
        override def set(t: Dmg, v: UByte): Dmg = normalLens.set(t, v & 0x30) //sets only bits 4 and 5
      }
      else if (addr.value == DMA) new Lens[Dmg,UByte] {
        override def get(t: Dmg): UByte = UByte.ZERO
        override def set(t: Dmg, v: UByte): Dmg = {
          //COPIES 160 bytes see DMA documentation
          @tailrec
          def go(dmg: Dmg, i: Int): Dmg = {
            val content = dmg.peek(Ind8(Imm16(v hl i)))
            val newDmg = dmg.poke(Ind8(Imm16(new U16(0xfe00 + i))), content)
            if (i == 0) newDmg else go(newDmg, i-1)
          }
          go(t, 159)
        }
      }
      else if (addr.value == LY) Lens.const[Dmg,UByte](new UByte(this.line))
      else if (addr.value == DIV) Lens.const[Dmg,UByte](new UByte(((this.cycleNr >> 8) % 0xff).toInt)) //Todo writing to it should reset it to zero... I wonder whether it's important
      else if (addr.value == TIMA) new LoggingLens(Lens.compose(Dmg.memLens, mem.lens(addr)), "unimplemented: TIMA")
      else if (addr.value == 0xff22) new LoggingLens(Lens.compose(Dmg.memLens, mem.lens(addr)), "unimplemented: NR43")

      else if (addr.value >= 0x8000 && addr.value <= 0xFFFF) Lens.compose(Dmg.memLens, mem.lens(addr))
      else null // TODO should really throw exception or use Option.
      //TODO there are other things! For example, cart RAM, etc. Implement.
    }
    case IndirectHi(Imm8(a)) => lens[UByte](Ind8(Imm16(new UByte(0xff) hl a)))
    case IndirectHi(r: DmgRegister8) => lens[UByte](Ind8(Imm16(new UByte(0xff) hl cpu.read(r))))
    case Ind8(r: DmgRegister16) => lens[UByte](Ind8(Imm16(cpu.read16(r))))
    case Ind16(r: DmgRegister16) => lens[U16](Ind16(Imm16(cpu.read16(r))))
    case Ind16(Imm16(addr)) => make16BitLens(lens[UByte](Ind8(Imm16(addr))), lens[UByte](Ind8(Imm16(addr + 1))))
    case _ => null //TODO wrong to use null!!!
  }).asInstanceOf[Lens[Dmg,A]]

  private def make16BitLens(l1: Lens[Dmg,UByte], l2: Lens[Dmg,UByte]): Lens[Dmg,U16] = new Lens[Dmg, U16]{
    override def get(t: Dmg): U16 = l2.get(t) hl l1.get(t)
    override def set(t: Dmg, v: U16): Dmg = l1.set(l2.set(t, v.hi), v.lo)
  }

  def bootRomEnabled: Boolean = peek(BOOT).isZero

  // convenience method to allow peeking by passing a simple U16 (address)
  def peek(address: U16): UByte = peek[UByte](Ind8(Imm16(address)))

  def peek[A](op: DmgOperand[A]): A = op match {
    case Imm8(v) => v.asInstanceOf[A]
    case Imm16(vv) => vv.asInstanceOf[A]
    case reg: DmgRegister[A] => reg match {
      case f: DmgRegisterFlag => this.cpu.readFlag(f).asInstanceOf[A]
      case r: DmgRegister8 => this.cpu.read(r).asInstanceOf[A]
      case r: DmgRegister16 => this.cpu.read16(r).asInstanceOf[A]
    }
    case _ => this.lens[A](op).get(this)
  }

  def poke[A](op: DmgOperand[A], value: A): Dmg = op match {
    case reg: DmgRegister[A] => reg match {
      case f: DmgRegisterFlag => this.copy(cpu = this.cpu.writeFlag(f, value.asInstanceOf[Boolean]))
      case r: DmgRegister8 => this.copy(cpu = this.cpu.write(r, value.asInstanceOf[UByte]))
      case r: DmgRegister16 => this.copy(cpu = this.cpu.write16(r, value.asInstanceOf[U16]))
    }
    case _ => this.lens[A](op).set(this, value)
  }

  def instrStream: Stream[UByte] = {
    val pc = cpu.pc.value
    def symbol(i: Int): UByte = this.peek[UByte](Ind8(Imm16(new U16(pc+i))))
    symbol(0) #:: symbol(1) #:: symbol(2) #:: Stream.empty
  }
  def setFlags(z: FlagAction, n: FlagAction, h: FlagAction, c: FlagAction): Dmg = {
    var cpu2 = cpu //TODO rewrite without var!
    cpu2 = z match {
      case NotAffected => cpu2
      case Setb => cpu2.zero(true)
      case Reset => cpu2.zero(false)
      case SetIf(b: Boolean) => cpu2.zero(b)
    }
    cpu2 = n match {
      case NotAffected => cpu2
      case Setb => cpu2.subtract(true)
      case Reset => cpu2.subtract(false)
      case SetIf(b: Boolean) => cpu2.subtract(b)
    }
    cpu2 = h match {
      case NotAffected => cpu2
      case Setb => cpu2.halfCarry(true)
      case Reset => cpu2.halfCarry(false)
      case SetIf(b: Boolean) => cpu2.halfCarry(b)
    }
    cpu2 = c match {
      case NotAffected => cpu2
      case Setb => cpu2.carry(true)
      case Reset => cpu2.carry(false)
      case SetIf(b: Boolean) => cpu2.carry(b)
    }
    this.copy(cpu=cpu2)
  }

  def fetch: (Option[DmgInstruction], Dmg) = {
    val pc = cpu.pc.value

    val instr: Option[DmgInstruction] = {
      def symbol(i: Int): UByte = this.peek[UByte](Ind8(Imm16(new U16(pc + i))))
      val b0 = symbol(0)
      lazy val b1 = symbol(1)
      lazy val b2 = symbol(2)
      TableBasedParser.parse(b0, b1, b2)
    }

    val length: Int = instr.map(_.length).getOrElse(1) // 1= default length for unknown instructions.

    // Advance the program counter
    val newDmg = this.copy(cpu = this.cpu.pc(this.cpu.pc + length))
    (instr, newDmg)
  }

  def advance: Dmg = {

    val interrupt = this.outstandingInterrupt
    val theDmg = interrupt match {
      case None => this
      case Some(intr) => {
        val unhalted = this.copy(cpu = this.cpu.copy(halted = false)) //unhalts the cpu if necessary
        unhalted.acknowledgeInterrupt(intr).callInterrupt(intr)
      }
    }

    val newDmg = if (!theDmg.cpu.halted) {
      val opc = peek[UByte](Ind8(PC)).value
      lazy val opc2 = peek[UByte](Ind8(Imm16(new U16(cpu.pc.value + 1)))).value

      val (instr, newDmg) = theDmg.fetch
      val (_, newDmg2) = instr.get.execute.run(newDmg) //TODO handle None case for instr.
      val x = newDmg2.copy(cycleNr = newDmg2.cycleNr + instr.get.cycles)

      if (this.input.select) println("["+this.frame+"] " + this.cpu.pc + " %s ".format(instr.get) + this.cpu)
      x
    } else {
      // if the CPU was halted
      theDmg.copy(cycleNr = theDmg.cycleNr + 20) //simply increment the cycle number
      // (the number of cycles to increment is taken simply as a tradeoff between efficiency and precision of interrupt timing)
    }

    //now check if any interrupt is to be thrown.

    val oldLcdcStatus = this.lcdcStatus
    val newLcdcStatus = newDmg.lcdcStatus

    val y: Dmg = {
      if (newLcdcStatus != oldLcdcStatus) {
        val stat = newDmg.peek(STAT)
        newLcdcStatus match {
          case HBlankStatus if stat.test(3) => newDmg.raiseInterrupt(LCDCStatusInterrupt)
          case VBlankStatus => {
            if (stat.test(4)) newDmg.raiseInterrupt(LCDCStatusInterrupt).raiseInterrupt(VBlankInterrupt)
            else newDmg.raiseInterrupt(VBlankInterrupt)
          }
          case OamAccess    if stat.test(5) => newDmg.raiseInterrupt(LCDCStatusInterrupt)
          case VramAccess   if stat.test(6) => newDmg.raiseInterrupt(LCDCStatusInterrupt)
          case _ => newDmg
        }
      } else {
        newDmg
      }
    }
    y
  }

  def isInterruptEnabled(interruptType: DmgInterruptType): Boolean = {
    val interruptEnable = this.peek[UByte](Ind8(Imm16(IE)))
    interruptEnable.test(interruptType.flagBitNr)
  }

  //this method intentionally returns only one interrupt, even if there are many raised at once.
  //It is by spec that we should only acknlowledge one at a time, the one with the highest priority.
  def outstandingInterrupt: Option[DmgInterruptType] = {
    if (this.peek[Boolean](IME) == false) None
    else {
      val interruptFlags = peek[UByte](Ind8(Imm16(IF)))
      val interruptEnable = peek[UByte](Ind8(Imm16(IE)))
      //ANDs the two registers so that any interrupts that are disabled will not be seen
      val maskedInterrupts = interruptFlags & interruptEnable
      /**/ if (maskedInterrupts.test(0)) Some(VBlankInterrupt)
      else if (maskedInterrupts.test(1)) Some(LCDCStatusInterrupt)
      else if (maskedInterrupts.test(2)) Some(TimerOverflowInterrupt)
      else if (maskedInterrupts.test(3)) Some(SerialTransferInterrupt)
      else if (maskedInterrupts.test(4)) Some(ButtonInterrupt)
      else None
    }
  }

  def raiseInterrupt(interruptType: DmgInterruptType): Dmg =
    // sets the bit in the Interrupt Flag register corresponding to the given interrupt type.
    Dmg.modify(Ind8(Imm16(IF)), _.set(interruptType.flagBitNr)).run(this)._2

  def acknowledgeInterrupt(interruptType: DmgInterruptType): Dmg =
    // clears the bit in the Interrupt Flag register corresponding to the given interrupt type.
    Dmg.modify(Ind8(Imm16(IF)), _.reset(interruptType.flagBitNr)).run(this)._2

  def callInterrupt(interruptType: DmgInterruptType): Dmg = {
    // disables Interrupt Master Enable as per interrupt procedure, and calls the interrupt address
    (DI.execute andThen CALL(Unconditional, Imm16(interruptType.address)).execute).run(this)._2
  }

  // A detailed state description for debug.
  def trace: String = {
    "DMG state\n" +
    "Cycle %10d, frame %2d, line %3d\n".format(cycleNr, frame, line) +
    cpu.toString + "\n" +
    "Registers:\n" +
    mem.registers.dump +
    "Interrupt enable: " + peek[UByte](Ind8(Imm16(IE)))
  }

}

object Dmg {
  val ClockSpeed: Int = 4194304
  val CyclesPerLine: Int = 456
  val LinesPerFrame: Int = 154
  val CyclesPerFrame: Int = CyclesPerLine * LinesPerFrame
  val FramesPerSecond: Double = 59.72750

  def startWithCart(cart: Cartridge, runBoot: Boolean = true): Dmg = {
    val initial = new Dmg(cart, DmgMem.Initial, DmgCpu.Initial, DmgInput.Neutral, cycleNr = 0)
    if (runBoot) initial else {
      //skip boot sequence: disable boot rom and set program counter to 0x0100
      initial.poke[U16](PC, 0x0100).poke[UByte](Ind8(Imm16(0xff50)), 0x01)
    }
  }

  def doNothing: State[Dmg,Unit] = State(s => ((), s))
  def read(op: DmgOperand[UByte]): State[Dmg,UByte] = State(s => (s.peek(op), s))
  def read16(op: DmgOperand[U16]): State[Dmg,U16] = State(s => (s.peek[U16](op), s))
  def write(op: DmgOperand[UByte], value: UByte): State[Dmg,Unit] = State(s => ((), s.poke(op, value)))
  def write16(op: DmgOperand[U16], value: U16): State[Dmg,Unit] = State(s => ((), s.poke[U16](op, value)))
  def copy(dest: DmgOperand[UByte], src: DmgOperand[UByte]): State[Dmg,Unit] = State(s => ((),s.poke(dest, s.peek(src))))
  def copy16(dest: DmgOperand[U16], src: DmgOperand[U16]): State[Dmg,Unit] = State(s => ((),s.poke[U16](dest, s.peek[U16](src))))
  def modify(op: DmgOperand[UByte], f: UByte => UByte): State[Dmg,UByte] = State(s => { val v = f(s.peek(op)); (v, s.poke(op, v)) })
  def modify16(op: DmgOperand[U16], f: U16 => U16): State[Dmg,U16] = State(s => { val v = f(s.peek[U16](op)); (v, s.poke[U16](op, v)) })
  def flags(z: FlagAction, n: FlagAction, h: FlagAction, c: FlagAction): State[Dmg,Unit] = State(s => {
    ((), s.setFlags(z,n,h,c))
  })//TODO implement better
  def getFlags: State[Dmg,Flags] = State(s => (new Flags(s.peek(F)), s))

  def check(cond: Condition): State[Dmg,Boolean] = State(s => (cond match {
    case Unconditional => true
    case  Z =>  s.cpu.zero
    case NZ => !s.cpu.zero
    case CS =>  s.cpu.carry
    case NC => !s.cpu.carry
  }, s))

  val cpuLens = new Lens[Dmg,DmgCpu] {
    def get(t: Dmg): DmgCpu = t.cpu
    def set(t: Dmg, v: DmgCpu): Dmg = t.copy(cpu = v)
  }

  val memLens = new Lens[Dmg,DmgMem] {
    def get(t: Dmg): DmgMem = t.mem
    def set(t: Dmg, v: DmgMem): Dmg = t.copy(mem = v)
  }

  val cartLens = new Lens[Dmg,Cartridge] {
    def get(t: Dmg): Cartridge = t.cart
    def set(t: Dmg, v: Cartridge): Dmg = t.copy(cart = v)
  }


  private val bootRomData: Vector[Byte] = Vector[Short](
    0x31, 0xFE, 0xFF, 0xAF, 0x21, 0xFF, 0x9F, 0x32, 0xCB, 0x7C, 0x20, 0xFB, 0x21, 0x26, 0xFF, 0x0E,
    0x11, 0x3E, 0x80, 0x32, 0xE2, 0x0C, 0x3E, 0xF3, 0xE2, 0x32, 0x3E, 0x77, 0x77, 0x3E, 0xFC, 0xE0,
    0x47, 0x11, 0x04, 0x01, 0x21, 0x10, 0x80, 0x1A, 0xCD, 0x95, 0x00, 0xCD, 0x96, 0x00, 0x13, 0x7B,
    0xFE, 0x34, 0x20, 0xF3, 0x11, 0xD8, 0x00, 0x06, 0x08, 0x1A, 0x13, 0x22, 0x23, 0x05, 0x20, 0xF9,
    0x3E, 0x19, 0xEA, 0x10, 0x99, 0x21, 0x2F, 0x99, 0x0E, 0x0C, 0x3D, 0x28, 0x08, 0x32, 0x0D, 0x20,
    0xF9, 0x2E, 0x0F, 0x18, 0xF3, 0x67, 0x3E, 0x64, 0x57, 0xE0, 0x42, 0x3E, 0x91, 0xE0, 0x40, 0x04,
    0x1E, 0x02, 0x0E, 0x0C, 0xF0, 0x44, 0xFE, 0x90, 0x20, 0xFA, 0x0D, 0x20, 0xF7, 0x1D, 0x20, 0xF2,
    0x0E, 0x13, 0x24, 0x7C, 0x1E, 0x83, 0xFE, 0x62, 0x28, 0x06, 0x1E, 0xC1, 0xFE, 0x64, 0x20, 0x06,
    0x7B, 0xE2, 0x0C, 0x3E, 0x87, 0xE2, 0xF0, 0x42, 0x90, 0xE0, 0x42, 0x15, 0x20, 0xD2, 0x05, 0x20,
    0x4F, 0x16, 0x20, 0x18, 0xCB, 0x4F, 0x06, 0x04, 0xC5, 0xCB, 0x11, 0x17, 0xC1, 0xCB, 0x11, 0x17,
    0x05, 0x20, 0xF5, 0x22, 0x23, 0x22, 0x23, 0xC9, 0xCE, 0xED, 0x66, 0x66, 0xCC, 0x0D, 0x00, 0x0B,
    0x03, 0x73, 0x00, 0x83, 0x00, 0x0C, 0x00, 0x0D, 0x00, 0x08, 0x11, 0x1F, 0x88, 0x89, 0x00, 0x0E,
    0xDC, 0xCC, 0x6E, 0xE6, 0xDD, 0xDD, 0xD9, 0x99, 0xBB, 0xBB, 0x67, 0x63, 0x6E, 0x0E, 0xEC, 0xCC,
    0xDD, 0xDC, 0x99, 0x9F, 0xBB, 0xB9, 0x33, 0x3E, 0x3C, 0x42, 0xB9, 0xA5, 0xB9, 0xA5, 0x42, 0x3C,
    0x21, 0x04, 0x01, 0x11, 0xA8, 0x00, 0x1A, 0x13, 0xBE, 0x20, 0xFE, 0x23, 0x7D, 0xFE, 0x34, 0x20,
    0xF5, 0x06, 0x19, 0x78, 0x86, 0x23, 0x05, 0x20, 0xFB, 0x86, 0x20, 0xFE, 0x3E, 0x01, 0xE0, 0x50).map(_.toByte).toVector
    // data is laid out as shorts for readability as the Byte type forces me into the signed representation

  val bootRom: Rom = new Rom(bootRomData)

}

trait FlagAction
case object Setb extends FlagAction
case object Reset extends FlagAction
case class SetIf(condition: Boolean) extends FlagAction
case object NotAffected extends FlagAction


sealed trait LCDCStatus
case object HBlankStatus extends LCDCStatus
case object VBlankStatus extends LCDCStatus
case object OamAccess extends LCDCStatus
case object VramAccess extends LCDCStatus

