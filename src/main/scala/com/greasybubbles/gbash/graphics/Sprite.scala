package com.greasybubbles.gbash.graphics

import com.greasybubbles.gbash.UByte
import com.greasybubbles.gbash.mem.MemoryRegion

class Sprite(data: MemoryRegion) {
  val x: Int = data(1).value
  val y: Int = data(0).value
  def screenPosX: Int = x - 8   // offset as per spec
  def screenPosY: Int = y - 16  // offset as per spec

  def visible = !((y == 0) || (y > 144+16) || (x == 0) || (x > 160+8)) //TODO HOWEVER hiding a sprite horizontally has side effects on the other sprites. Implement!
  def patternNr: Int = data(2).value
  def priority: Boolean = data(3).test(7)
  def flipY: Boolean = data(3).test(6)
  def flipX: Boolean = data(3).test(5)
  def paletteNr: Int = if (data(3).test(4)) 1 else 0

  def isOnLine(lineNr: Int, is8x16: Boolean): Boolean = {
    val height = if (is8x16) 16 else 8
    (screenPosY <= lineNr) && (screenPosY + height > lineNr)
  }

  def isOnColumn(columnNr: Int): Boolean = {
    val width = 8
    (screenPosX <= columnNr) && (screenPosX + width > columnNr)
  }

  def pixel(u: Int, v: Int, is8x16: Boolean)(patternData: Tiledata): ColorIndex = {
    if (!is8x16) {
      val uu = if (flipX) 7 - u else u
      val vv = if (flipY) 7 - v else v
      val tile = patternData.tile(patternNr)
      tile.pixelCode(uu, vv)
    } else {
      val uu = if (flipX) 7 - u else u
      val vv = if (flipY) 15 - v else v
      val tile = patternData.tile(new UByte((patternNr & 0xfe) + (if (vv >= 8) 1 else 0)))
      tile.pixelCode(uu, vv % 8)
    }
  }
}
