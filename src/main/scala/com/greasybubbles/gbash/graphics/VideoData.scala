package com.greasybubbles.gbash.graphics

import com.greasybubbles.gbash.UByte
import com.greasybubbles.gbash.mem.MemoryRegion

case class VideoData(vram: MemoryRegion, oamData: MemoryRegion, vregs: MemoryRegion) {

  private val Tilemap1Address = 0x9800
  private val Tilemap2Address = 0x9C00
  private val TilemapSize = Tilemap.TilemapHeight * Tilemap.TilemapWidth

  private val PatternsPerTable = 256
  private val BytesPerPattern = 8
  private val PatternTableByteSize = PatternsPerTable * BytesPerPattern // 0x1000

  private val TileData1Address = 0x8000
  private val TileData2Address = 0x8800
  private val TileDataSize = PatternTableByteSize

  private val SpritePatternTableAddress = 0x8000 // 0x8000 - 0x8FFF
  private val SpritePatternTableSize = PatternTableByteSize

  private val OAMAddress = 0xFE00
  private val OAMSize = 0xA0

  lazy val tilemap1 = new Tilemap(vram.subregionOrg(Tilemap1Address, TilemapSize))
  lazy val tilemap2 = new Tilemap(vram.subregionOrg(Tilemap2Address, TilemapSize))

  def backgroundTilemap = if (LCDC.test(3)) tilemap2 else tilemap1
  def windowTilemap = if (LCDC.test(6)) tilemap2 else tilemap1

  lazy val tiledata1 = new Tiledata(vram.subregionOrg(TileData1Address, TileDataSize), signedIndices = false)
  lazy val tiledata2 = new Tiledata(vram.subregionOrg(TileData2Address, TileDataSize), signedIndices = true)

  def tiledata = if (LCDC.test(4)) tiledata1 else tiledata2
  def spritePatternData = tiledata1 //always 0x8000-0xffff as per spec

  lazy val oam = new ObjectAttributeMemory(oamData.subregionOrg(OAMAddress, OAMSize))

  def LCDC: UByte = vregs(0)
  def SCY:  UByte = vregs(2)
  def SCX:  UByte = vregs(3)
  def BGP:  UByte = vregs(7)
  def OBP0: UByte = vregs(8)
  def OBP1: UByte = vregs(9)

  lazy val backgroundPalette = Palette.decode(BGP, transparency = false)
  lazy val spritePalette0 = Palette.decode(OBP0, transparency = true)
  lazy val spritePalette1 = Palette.decode(OBP1, transparency = true) //DEBUG! should be false (???)
  lazy val spritePalette = List(spritePalette0, spritePalette1)
}