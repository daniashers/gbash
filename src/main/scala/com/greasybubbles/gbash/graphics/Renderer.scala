package com.greasybubbles.gbash.graphics

import scala.annotation.tailrec

object Renderer {

  val EmptyLine = Vector.fill(160)(Lightest)

  def renderAll(vr: VideoData): ScreenBuffer = {
    //val t0 = System.nanoTime()
    val s = new ScreenBuffer(Vector.tabulate(144)(renderLine(_)(vr)))
    //println("Rendering done in %d us".format((System.nanoTime()-t0)/1000))
    s
  }

  def renderLine(l: Int)(vr: VideoData): Vector[DmgColor] = {
    //if the screen is off, don't bother calculating anything and display a white line
    if (!vr.LCDC.test(7)) EmptyLine else {

      val spritesOnLine = vr.oam.spritesOnLine(l, vr.LCDC.test(2)).sortWith((a,b) => a.x < b.x) //sort by x coord to determine priority
      Vector.tabulate(160)(renderPixel(_, l)(spritesOnLine)(vr))
    }
  }

  def renderPixel(x: Int, y: Int)(spriteList: List[Sprite])(vr: VideoData): DmgColor = {
    // please note that it is assumed that spriteList is arranged in order of sprite priority

    //TODO - Window not yet implemented!

    // 1. Determine if sprite, window or background.

    //  - Determine sprites on that line.
    //  - Determine which are visible.
    //  - Check if any of them at position.

    val isDoubleHeightSprites = vr.LCDC.test(2)

    //this allows the list of sprites to not be recalculated
    val sprites = if (spriteList == null) vr.oam.spritesOnLine(y, isDoubleHeightSprites /* sprite height */) else spriteList
    val spritesOnPixel = sprites.filter(_.isOnColumn(x))

    if (vr.LCDC.test(1) && (spritesOnPixel.size > 0)) { //check if sprites are enabled at all and if there are any on the line
      @tailrec
      def renderSpritez(n: Int): DmgColor = {
      //this tries each sprite in turn, if transparent it tries the next priority sprite.
        if (n == spritesOnPixel.size) Transparent else {
          val color = renderSprite(spritesOnPixel(n), x, y, isDoubleHeightSprites)(vr)
          if (color != Transparent) color else renderSpritez(n+1)
        }
      }
      val color = renderSpritez(0)
      if (color == Transparent) renderBackground(x, y)(vr) else color
    } else if (vr.LCDC.test(0)) { //is background enabled?
      renderBackground(x, y)(vr)
    } else Lightest //default colour

  }

  def renderSprite(sprite: Sprite, x: Int, y: Int, is8x16: Boolean)(vr: VideoData): DmgColor = {
    //TODO consider moving the following logic to the SpriteBlock class
    val spriteu = x - sprite.screenPosX
    val spritev = y - sprite.screenPosY
    val colorCode = sprite.pixel(spriteu, spritev, is8x16)(vr.spritePatternData)

    vr.spritePalette(sprite.paletteNr).color(colorCode)
  }

  def renderBackground(x:Int, y:Int)(vr: VideoData): DmgColor = {

    // - Determine which background in use.
    // - Get scroll position
    // - Determine which tile and which x/y.
    // - Determine which tilemap in use.
    // - Get tile.
    // - Get pixel.

    val tilemapx = (x + vr.SCX).value % 256
    val tilemapy = (y + vr.SCY).value % 256
    val tilenrx = tilemapx / 8
    val tilenry = tilemapy / 8
    val tileu = tilemapx % 8
    val tilev = tilemapy % 8
    val tile = vr.backgroundTilemap.tile(tilenrx,tilenry)(vr.tiledata)
    val colorCode = tile.pixelCode(tileu,tilev)
    vr.backgroundPalette.color(colorCode)
  }
}