package com.greasybubbles.gbash.graphics

import com.greasybubbles.gbash.UByte
import com.greasybubbles.gbash.mem.MemoryRegion

object DmgScreen {
  val Width = 160
  val Height = 144
}

class ScreenBuffer(val lines: Vector[Vector[DmgColor]]) {
  def pixel(x: Int, y: Int): DmgColor = lines(y)(x)
}

class Tile(bintile: MemoryRegion) {
  def pixelCode(x: Int, y: Int): ColorIndex = {
    val byte1 = bintile(y*2)
    val byte2 = bintile(y*2 + 1)
    val bit = 7-x
    val index = (if (byte1.test(bit)) 1 else 0) + (if (byte2.test(bit)) 2 else 0)
    ColorIndex.Indices(index)
  }
}

class Tilemap(bindata: MemoryRegion) {
  import com.greasybubbles.gbash.graphics.Tilemap._
  def tileIndex(x: Int, y: Int): Int = bindata(x+y*TilemapWidth).value
  def tile(x: Int, y: Int)(td: Tiledata): Tile = td.tile(tileIndex(x,y))
}

object Tilemap {
  val TilemapHeight = 32
  val TilemapWidth = 32
}

class Tiledata(bindata: MemoryRegion, signedIndices: Boolean) {
  //TODO for the moment only handles unsigned tile numbers
  val BytesPerTile = 16
  def tile(index: UByte): Tile = {
    val i = if (signedIndices) (index.signedValue + 0x80) else index.value
    new Tile(bindata.subregion(BytesPerTile * i, BytesPerTile))
  }
}