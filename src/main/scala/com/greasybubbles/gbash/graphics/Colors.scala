package com.greasybubbles.gbash.graphics

import com.greasybubbles.gbash.UByte

sealed trait ColorIndex
case object Col0 extends ColorIndex
case object Col1 extends ColorIndex
case object Col2 extends ColorIndex
case object Col3 extends ColorIndex

object ColorIndex {
  val Indices: List[ColorIndex] = List(Col0, Col1, Col2, Col3)
}

sealed trait DmgColor
case object Lightest extends DmgColor
case object LightGray extends DmgColor
case object DarkGray extends DmgColor
case object Darkest extends DmgColor
case object Transparent extends DmgColor

class Palette(a: DmgColor, b: DmgColor, c: DmgColor, d: DmgColor) {
  def color(i: ColorIndex): DmgColor = i match {
    case Col0 => a
    case Col1 => b
    case Col2 => c
    case Col3 => d
  }
}
object Palette {
  val Colors = List(Lightest, LightGray, DarkGray, Darkest)
  def decode(code: UByte, transparency: Boolean): Palette = {
    val code1 = ((code >> 0) & 0x03).value
    val code2 = ((code >> 2) & 0x03).value
    val code3 = ((code >> 4) & 0x03).value
    val code4 = ((code >> 6) & 0x03).value
//    val color1 = if ((code1 == 0) && transparency) Transparent else Colors(code1)
//    val color2 = if ((code2 == 0) && transparency) Transparent else Colors(code2)
//    val color3 = if ((code3 == 0) && transparency) Transparent else Colors(code3)
//    val color4 = if ((code4 == 0) && transparency) Transparent else Colors(code4)
    val color1 = if (transparency) Transparent else Colors(code1)
    val color2 = Colors(code2)
    val color3 = Colors(code3)
    val color4 = Colors(code4)
    new Palette(color1, color2, color3, color4)
  }
}
