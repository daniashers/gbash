package com.greasybubbles.gbash.graphics

import com.greasybubbles.gbash.mem.MemoryRegion

//requires a memoryRegion of size 0xA0 to be passed in
class ObjectAttributeMemory(oamData: MemoryRegion) {
  private val NrOfSprites = 40
  private val BytesPerSpriteBlock = 4

  val sprites: List[Sprite] =
    List.tabulate(40)(i => new Sprite(oamData.subregion(i * BytesPerSpriteBlock, BytesPerSpriteBlock)))

  def spritesOnLine(lineNr: Int, is8x16: Boolean): List[Sprite] = sprites.filter(_.isOnLine(lineNr, is8x16))
  // TODO the sprites returned above should be restricted to 10 as per spec and ordered by priority
}