package com.greasybubbles.gbash.debug

import com.greasybubbles.gbash.{Dmg, U16}

sealed trait Breakpoint

case class AddressBreakpoint(addr: U16) extends Breakpoint
case class ConditionBreakpoint(cond: Dmg => Boolean) extends Breakpoint
