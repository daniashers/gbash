package com.greasybubbles.gbash

import java.awt.event.{KeyEvent, KeyListener}

import com.greasybubbles.gbash.graphics._
import com.greasybubbles.gbash.input.{KeyBindings, DmgInput}
import com.greasybubbles.gbash.parsers.Diagno

import math._
import scala.annotation.tailrec
import scala.swing.event.{KeyReleased, Key, KeyPressed}
import scala.swing.{Swing, Panel, MainFrame, SimpleSwingApplication}
import java.awt.{Color, Graphics2D, Dimension}

class RasterDisplay(var data: Array[Array[Color]]) extends Panel {

  override def paintComponent(g: Graphics2D) {
    val dx = g.getClipBounds.width.toFloat  / data.length
    val dy = g.getClipBounds.height.toFloat / data.map(_.length).max
    for {
      x <- 0 until data.length
      y <- 0 until data(x).length
      x1 = (x * dx).toInt
      y1 = (y * dy).toInt
      x2 = ((x + 1) * dx).toInt
      y2 = ((y + 1) * dy).toInt
    } {
      data(x)(y) match {
        case c: Color => g.setColor(c)
        case _ => g.setColor(Color.WHITE)
      }
      g.fillRect(x1, y1, x2 - x1, y2 - y1)
    }
  }
}

object Main {
  def main(args: Array[String]): Unit = {
    val filename = args(0)
    val runBoot = !args.contains("--noboot")
    new GbashMain(filename, runBoot).main(args)
  }
}

// Please do not pay attention to how badly the side-effecting part of the application is structured!
// Full of impure functions etc. I know this class should be cleaned up.
class GbashMain(filename: String, runBootRom: Boolean) extends SimpleSwingApplication {

  var dmg: Dmg = {
    val cart = Cartridge.fromFile(filename)
    Dmg.startWithCart(cart, runBootRom)
  }

  // I know it's naive to just wait a set time in order to deliver frames in a precise timing.
  // For the time being I will leave it like this, though.
  val timer = new javax.swing.Timer(17, Swing.ActionListener(e =>
  {
    doFrame
  }))

  //val data = DemoDataGen.data
  val data = DmgVideoOutput.data(dmg)

  var dp = new RasterDisplay(data) {
    val scale = 3
    preferredSize = new Dimension(160 * scale, 144 * scale)
  }

  var input: DmgInput = DmgInput.Neutral

  def top = new MainFrame {
    timer.start()
    contents = dp

    this.peer.addKeyListener(new KeyListener() {
      override def keyTyped(e: KeyEvent): Unit = ()
      override def keyPressed(e: KeyEvent): Unit = {
        if (e.getKeyCode == 80) {
          timer.stop()
          println (Diagno.dump)
                    //println (dmg.mem.videoData.oamData.dump) //TODO debug
        }
        else if (e.getKeyCode == 79) timer.start()
        else input = e.getKeyCode match {
          case KeyBindings.Start => input.copy(start=true)
          case KeyBindings.Select => input.copy(select=true)
          case KeyBindings.A => input.copy(a=true)
          case KeyBindings.B => input.copy(b=true)
          case KeyBindings.Up => input.copy(up=true)
          case KeyBindings.Down => input.copy(down=true)
          case KeyBindings.Left => input.copy(left=true)
          case KeyBindings.Right => input.copy(right=true)
          case _ => input
        }
      }
      override def keyReleased(e: KeyEvent): Unit = {input = e.getKeyCode match {
        case KeyBindings.Start => input.copy(start=false)
        case KeyBindings.Select => input.copy(select=false)
        case KeyBindings.A => input.copy(a=false)
        case KeyBindings.B => input.copy(b=false)
        case KeyBindings.Up => input.copy(up=false)
        case KeyBindings.Down => input.copy(down=false)
        case KeyBindings.Left => input.copy(left=false)
        case KeyBindings.Right => input.copy(right=false)
        case _ => input}
      }
    })
  }

  @tailrec
  final def runTillVblank(dmg: Dmg): Dmg = {
    val wasVblank = dmg.vblank
    val newDmg = dmg.advance
    val isVblank = newDmg.vblank
    if (isVblank && !wasVblank) newDmg else runTillVblank(newDmg)
  }


  def doFrame: Unit = {
    val ti = System.nanoTime()

    dp.data = DmgVideoOutput.data(dmg)
    dp.repaint()

    val t0 = System.nanoTime()
    val cyc0 = dmg.cycleNr

    dmg = runTillVblank(dmg).copy(input = this.input)

    val t1 = System.nanoTime()
    val cyc1 = dmg.cycleNr
    //println("Render time: %d, cpu time %d".format(t0-ti, t1-t0))
    if (t0%10000 == 0)
    println("Speed: %.2f MHz (%.1f ms)".format((1000.0f * (cyc1-cyc0).toFloat / (t1-t0).toFloat), (t1-t0).toFloat/1000000.0f))
  }
}


object DmgVideoOutput {
  val Col0 = new Color(.85f,.9f,.8f)
  val Col1 = new Color(.65f,.7f,.6f)
  val Col2 = new Color(.45f,.5f,.4f)
  val Col3 = new Color(.25f,.3f,.2f)

  def realColor(c: DmgColor): Color = c match {
    case Lightest  => Col0
    case LightGray => Col1
    case DarkGray  => Col2
    case Darkest   => Col3
    case Transparent => new Color(1f,0f,1f)
  }

  def data(dmg: Dmg) = {
    val screenBuffer = Renderer.renderAll(dmg.mem.videoData)
    Array.tabulate[Color](160, 144)((x,y) => realColor(screenBuffer.pixel(x,y)))
  }
}

object DemoDataGen {

  val Col0 = new Color(.85f,.9f,.8f)
  val Col1 = new Color(.65f,.7f,.6f)
  val Col2 = new Color(.45f,.5f,.4f)
  val Col3 = new Color(.25f,.3f,.2f)

  val Colors = List(Col0,Col1,Col2,Col3)

  val data = Array.tabulate[Color](160, 144)((x,y) => Colors(Math.floor(2+(Math.sin(0.2*x)+Math.sin(0.2*y))).toInt))

}