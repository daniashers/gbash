package com.greasybubbles.gbash

import scala.annotation.tailrec

object U16 {
  implicit def IntToU16(value: Int): U16 = new U16(value)
}

class U16(val initValue: Int) extends AnyVal {
  def maxValue = 0xffff
  def modulo = maxValue + 1
  def value = (initValue % modulo + modulo) % modulo
  def hi = new UByte((this.value >> 8) & 0xff)
  def lo = new UByte(this.value & 0xff)

  def +(that: U16): U16 = {
    val sum = (this.value + that.value)
    new U16(sum) //TODO implement flags
  }
  def -(that: U16): U16 = {
    val difference = (this.value - that.value)
    new U16(difference) //TODO implement flags
  }
  //signed add
  def +-(that: UByte): U16 = {
    val sum = (this.value + that.signedValue)
    val carry = sum > maxValue;
    val hCarry = (this.value & 0xf + that.value & 0xf) > 0xf
    new U16(sum % modulo)
  }
  override def toString = "$%04x".format(value)
}






class UByte(val initValue: Int) extends AnyVal {
  def maxValue = 0xff
  def modulo = maxValue + 1
  def value = (initValue % modulo + modulo) % modulo //to ensure modulo is positive
  def signedValue: Byte = {val v = value % modulo; (if (v >= 0x80) (v - modulo) else v).toByte }
  def +(that: UByte): UByte = new UByte((this.value + that.value) % modulo)
  def -(that: UByte): UByte = new UByte(this.value - that.value)
  def <<(bits: Int): UByte = new UByte(this.value << bits)
  def >>(bits: Int): UByte = new UByte(this.value >> bits)
  def hl(that: UByte): U16 = new U16(this.value << 8 | that.value)
  def &(that: UByte): UByte = new UByte(this.value & that.value)
  def |(that: UByte): UByte = new UByte(this.value | that.value)
  def unary_~ : UByte = new UByte(~value & 0xff)
  def xor(that: UByte): UByte = new UByte(this.value ^ that.value)
  def set(bitNr: Int): UByte = new UByte(this.value | (1 << bitNr))
  def reset(bitNr: Int): UByte = new UByte(this.value & ~(1 << bitNr))
  def bit(bitNr: Int, state: Boolean): UByte = if (state) set(bitNr) else reset(bitNr)
  def test(bitNr: Int): Boolean = (this.value & (1 << bitNr)) != 0
  def msb: Boolean = this.test(7)
  def lsb: Boolean = this.test(0)
  def lowNybble: Int = value & 0x0f
  def highNybble: Int = (value >> 4) & 0x0f
  def isZero = this.value == 0

  //add with carry
  def adc(that: UByte, oldCarry: Boolean = false): (UByte, Boolean, Boolean) = {
    val sum = (this.value + that.value) + (if (oldCarry) 1 else 0)
    val carry = sum > maxValue;
    val halfCarry = ((this.value & 0xf) + (that.value & 0xf)) > 0xf
    (new UByte(sum % modulo), carry, halfCarry)
  }
  //subtract with borrow
  def sbc(that: UByte, oldBorrow: Boolean = false): (UByte, Boolean, Boolean) = {
    val difference = (this.value - that.value) - (if (oldBorrow) 1 else 0)
    val borrow = (difference < 0)
    val halfBorrow = ((this.value & 0xf) - (that.value & 0xf)) < 0
    (new UByte(difference), borrow, halfBorrow)
  }
  override def toString = "$%02x".format(value)

}

object UByte {
  def apply(v: Int): UByte = new UByte(v)
  implicit def IntToUByte(intValue: Int): UByte = new UByte(intValue)
  //implicit def UByteToInt(uByteValue: UByte): Int = uByteValue.value

  def fromSigned(signedByteValue: Byte) = new UByte(signedByteValue.toInt + (if (signedByteValue<0) 256 else 0))
  val ZERO = new UByte(0)
}