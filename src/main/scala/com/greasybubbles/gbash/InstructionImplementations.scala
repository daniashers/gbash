package com.greasybubbles.gbash

import com.greasybubbles.gbash.DmgInstruction._
import com.greasybubbles.gbash.Dmg._
import com.greasybubbles.gbash.State._
import com.greasybubbles.gbash.cpu._

abstract class DmgInstruction extends CpuInstruction[Dmg]

object DmgInstruction {
  def operandLength[A](oper: DmgOperand[A]): Int = oper match {
    case Imm8(_) => 1
    case Imm16(_) => 2
    case IndirectHi(Imm8(_)) => 1
    case Ind8(Imm16(_)) => 2
    case Ind16(Imm16(_)) => 2
    case _ => 0
  }
  def operandExtraCycles[A](oper: DmgOperand[A]): Int = oper match {
    case Imm8(_) => 4
    case Ind8(_) => 4
    case Ind16(_) => 4 //TODO is this true?
    case _ => 0
  }
}

case class Undocumented(opcode: UByte) extends DmgInstruction {
  val length = 1
  val cycles = 0
  val mnemonic = "??? (" + opcode + ")"
  override def execute: State[Dmg, Unit] = new State[Dmg,Unit] {
    override def run(s: Dmg): (Unit, Dmg) = {
      println("Warning: Undocumented opcode executed: " + mnemonic)
      ((),s)
    }
  }
}

case class NotYetImplemented(opcode: UByte) extends DmgInstruction {
  val length = 1
  val cycles = 0
  val mnemonic = "NoImpl (" + opcode + ")"
  override def execute: State[Dmg, Unit] = new State[Dmg,Unit] {
    override def run(s: Dmg): (Unit, Dmg) = {
      println("Warning: Unimplemented opcode executed: " + mnemonic)
      ((),s)
    }
  }
}

case object NOP extends DmgInstruction {
  val length = 1
  val cycles = 4
  val mnemonic = "NOP"
  val execute: State[Dmg,Unit] = State(s => ((),s))
}

case class LD(dest: DmgOperand[UByte], src: DmgOperand[UByte]) extends DmgInstruction {
  val length = 1 + operandLength(dest) + operandLength(src)
  val cycles = 4 + 0 //TODO sort out cycle difference when indirect operands, immediates, etc...
  val mnemonic = "LD"
  override def toString = mnemonic + " " + dest + "," + src
  def execute: State[Dmg,Unit] = Dmg.copy(dest, src) //flags not affected
}

case class PUSH(reg: DmgRegister[U16]) extends DmgInstruction {
  val length = 1
  val cycles = 16
  val mnemonic = "PUSH"
  override def toString = mnemonic + " " + reg
  def execute: State[Dmg,Unit] = for {
    _ <- modify16(SP, _ - 2) //decrement stack pointer
    _ <- Dmg.copy16(Ind16(SP), reg)
  } yield ()
}

case class POP(reg: DmgRegister[U16]) extends DmgInstruction {
  val length = 1
  val cycles = 16
  val mnemonic = "POP"
  override def toString = mnemonic + " " + reg
  def execute: State[Dmg,Unit] = for {
    _ <- Dmg.copy16(reg, Ind16(SP))
    _ <- modify16(SP, _ + 2) //increment stack pointer
  } yield ()
}

case class INC(reg: DmgOperand[UByte]) extends DmgInstruction {
  val length = 1
  val cycles = if (reg == Ind8(HL)) 12 else 4
  val mnemonic = "INC"
  override def toString = mnemonic + " " + reg
  def execute: State[Dmg,Unit] = for {
    value <- read(reg)
    (newVal, carry, halfCarry) = value adc 1
    _ <- write(reg, newVal)
    _ <- flags(z = SetIf(newVal.isZero), n = Reset, h = SetIf(halfCarry), c = NotAffected)
  } yield ()
}

case class DEC(reg: DmgOperand[UByte]) extends DmgInstruction {
  val length = 1
  val cycles = if (reg == Ind8(HL)) 12 else 4
  val mnemonic = "DEC"
  override def toString = mnemonic + " " + reg
  def execute: State[Dmg,Unit] = new State[Dmg,Unit] {
    override def run(s: Dmg): (Unit, Dmg) = {
      val v = s.peek(reg)
      val (newVal, carry, halfCarry) = v sbc 1
      val newS = s.poke(reg, newVal)
      val newCpu = newS.cpu.zero(newVal.isZero).subtract(true).halfCarry(!halfCarry) //carry not affected
      ((), newS.copy(cpu = newCpu))
    }
  }
}


case class LDD(dest: DmgOperand[UByte], src: DmgOperand[UByte]) extends DmgInstruction {
  val length = 1
  val cycles = 8
  val mnemonic = "LDD"
  override def toString = mnemonic + " " + dest + ", " + src
  def execute: State[Dmg,Unit] = LD(dest,src).execute andThen DEC16(HL).execute
}

case class LDI(dest: DmgOperand[UByte], src: DmgOperand[UByte]) extends DmgInstruction {
  val length = 1
  val cycles = 8
  val mnemonic = "LDI"
  override def toString = mnemonic + " " + dest + ", " + src
  def execute: State[Dmg,Unit] = LD(dest,src).execute andThen INC16(HL).execute
}

case class LDH(dest: DmgOperand[UByte], src: DmgOperand[UByte]) extends DmgInstruction {
  val length = 1 + operandLength(dest) + operandLength(src)
  val cycles = 8 + 0 //TODO sort out cycle difference when indirect operands, immediates, etc...
  val mnemonic = "LD"
  override def toString = mnemonic + " " + dest + "," + src
  val execute: State[Dmg,Unit] = Dmg.copy(dest, src) //flags not affected
}

//16-BIT INSTRUCTIONS
case class LD16(reg: DmgOperand[U16], value: DmgOperand[U16]) extends DmgInstruction {
  val length = if (value == HL) 1 else 3
  val cycles = if (value == HL) 8 else 12
  val mnemonic = "LD"
  override def toString = mnemonic + " " + reg + ", " + value
  def execute: State[Dmg,Unit] = copy16(reg, value)
}

case class ADDHL(reg: DmgRegister[U16]) extends DmgInstruction {
  val length = 1
  val cycles = 8
  val mnemonic = "ADD"
  override def toString = mnemonic + " HL, " + reg
  def execute: State[Dmg,Unit] = new State[Dmg,Unit] {
    override def run(s: Dmg): (Unit, Dmg) = {
      val hl = s.peek[U16](HL)
      val addend = s.peek[U16](reg)
      val sum = hl + addend
      val carry = (hl.value + addend.value) > 0xffff
      val halfCarry = ((hl.hi.value & 0x0f) + (addend.hi.value & 0x0f)) > 0x0f
      val newCpu = s.cpu.subtract(false).halfCarry(halfCarry).carry(carry)
      ((), s.copy(cpu = newCpu).poke[U16](HL, sum))
    }
  }
}

case class ADDSP(immValue: Int) extends DmgInstruction {
  val length = 1
  val cycles = 8
  val mnemonic = "ADD"
  override def toString = mnemonic + " SP, " + immValue
  def execute: State[Dmg,Unit] = new State[Dmg,Unit] {
    override def run(s: Dmg): (Unit, Dmg) = {
      val sp = s.peek[U16](SP)
      val sum = new U16(sp.value + immValue)
      val carry = (sp.lo.value + immValue) > 0xff // ?
      val halfCarry = ((sp.lo.value & 0x0f) + (immValue & 0x0f)) > 0x0f // ?
      val newCpu = s.cpu.subtract(false).halfCarry(halfCarry).carry(carry)
      ((), s.copy(cpu = newCpu).poke[U16](SP, sum))
    } //TODO couldn't find too much documentation on this instruction, totally guessing the behaviour of the flags.
  }
}

case class INC16(reg: DmgRegister[U16]) extends DmgInstruction {
  val length = 1
  val cycles = 8
  val mnemonic = "INC"
  override def toString = mnemonic + " " + reg
  def execute: State[Dmg,Unit] = for {
    v <- read16(reg)
    result = v + 1
    _ <- write16(reg, result)
    //no flags affected.
  } yield ()
}

case class DEC16(reg: DmgRegister16) extends DmgInstruction {
  val length = 1
  val cycles = 8
  val mnemonic = "DEC"
  override def toString = mnemonic + " " + reg
  def execute: State[Dmg,Unit] = new State[Dmg,Unit] {
    override def run(s: Dmg): (Unit, Dmg) = {
      ((), s.copy(cpu = s.cpu.write16(reg, s.cpu.read16(reg) - 1)))
      //no flags affected
    }
  }

//    v <- read16(reg)
//    result = v - 1
//    _ <- write16(reg, result)

}

abstract case class LD16IND(addr: Ind[U16], src: DmgRegister[U16]) extends DmgInstruction
abstract case class LDHL_SP(offs: Imm8) extends DmgInstruction
abstract case class ADD16IMM(reg: DmgRegister[U16], value: Imm8) extends DmgInstruction

//8-BIT ARITHMETIC
case class ADD(reg: DmgOperand[UByte]) extends DmgInstruction {
  val length = 1 + operandLength(reg)
  val cycles = 4 + operandExtraCycles(reg)
  val mnemonic = "ADD"
  override def toString = mnemonic + " A,"+reg
  def execute: State[Dmg,Unit] = for {
    a <- read(A)
    o <- read(reg)
    (result, carry, halfCarry) = a adc o
    _ <- write(A, result)
    _ <- flags(z = SetIf(result.isZero), n = Reset, h = SetIf(halfCarry), c = SetIf(carry))
  } yield ()
}
case class ADC(reg: DmgOperand[UByte]) extends DmgInstruction  {
  val length = 1 + operandLength(reg)
  val cycles = 4 + operandExtraCycles(reg)
  val mnemonic = "ADC"
  def execute: State[Dmg,Unit] = for {
    a <- read(A)
    o <- read(reg)
    f <- getFlags
    (result, carry, halfCarry) = a adc(o, oldCarry = f.C)
    _ <- write(A, result)
    _ <- flags(z = SetIf(result.isZero), n = Reset, h = SetIf(halfCarry), c = SetIf(carry))
  } yield ()
}

case class SUB(reg: DmgOperand[UByte]) extends DmgInstruction  {
  val length = 1 + operandLength(reg)
  val cycles = 4 + operandExtraCycles(reg)
  val mnemonic = "SUB"
  val execute: State[Dmg,Unit] = new State[Dmg,Unit] {
    override def run(s: Dmg): (Unit, Dmg) = {
      val (result, borrow, halfBorrow) = s.cpu.a sbc s.peek(reg)
      val newCpu = s.cpu.copy(a = result).zero(result.isZero).subtract(true).halfCarry(halfBorrow).carry(borrow)
      ((), s.copy(cpu = newCpu))
    }
  }
}

case class SBC(reg: DmgOperand[UByte]) extends DmgInstruction  {
  val length = 1 + operandLength(reg)
  val cycles = 4 + operandExtraCycles(reg)
  val mnemonic = "SBC"
  def execute: State[Dmg,Unit] = for {
    a <- read(A)
    o <- read(reg)
    f <- getFlags
    (result, borrow, halfBorrow) = a sbc(o, oldBorrow = !f.C)
    _ <- write(A, result)
    _ <- flags(z = SetIf(result.isZero), n = Setb, h = SetIf(halfBorrow), c = SetIf(borrow))
  } yield ()
}

case class AND(reg: DmgOperand[UByte]) extends DmgInstruction  {
  val length = 1 + operandLength(reg)
  val cycles = 4 + operandExtraCycles(reg)
  val mnemonic = "AND"
  val execute: State[Dmg,Unit] = new State[Dmg,Unit] {
    override def run(s: Dmg): (Unit, Dmg) = {
      val a = s.cpu.a
      val o = s.peek(reg)
      val result = a & o
      val newCpu = s.cpu.copy(a = result).zero(result.isZero).subtract(false).halfCarry(true).carry(false)
      ((), s.copy(cpu = newCpu))
    }
  }
}

case class XOR(reg: DmgOperand[UByte]) extends DmgInstruction  {
  val length = 1 + operandLength(reg)
  val cycles = 4 + operandExtraCycles(reg)
  val mnemonic = "XOR"
  val execute: State[Dmg,Unit] = new State[Dmg,Unit] {
    override def run(s: Dmg): (Unit, Dmg) = {
      val a = s.cpu.a
      val o = s.peek[UByte](reg)
      val result = a xor o
      val newCpu = s.cpu.copy(a = result).zero(result.isZero).subtract(false).halfCarry(false).carry(false)
      ((), s.copy(cpu = newCpu))
    }
  }
}

case class OR (reg: DmgOperand[UByte]) extends DmgInstruction  {
  val length = 1 + operandLength(reg)
  val cycles = 4 + operandExtraCycles(reg)
  val mnemonic = "OR"
  def execute: State[Dmg,Unit] = for {
    a <- read(A)
    o <- read(reg)
    result: UByte = a | o
    _ <- write(A, result)
    _ <- flags(z = SetIf(result.isZero), n = Reset, h = Reset, c = Reset)
  } yield ()
}
case class CP (reg: DmgOperand[UByte]) extends DmgInstruction  {
  val length = 1 + operandLength(reg)
  val cycles = 4 + operandExtraCycles(reg)
  val mnemonic = "CP"
  def execute: State[Dmg,Unit] = new State[Dmg,Unit] {
    override def run(s: Dmg): (Unit, Dmg) ={
      val a = s.cpu.a
      val o = s.peek(reg)
      val (result, borrow, halfBorrow) = a sbc o
      val newCpu = s.cpu.zero(result.isZero).subtract(true).halfCarry(halfBorrow).carry(borrow)
      ((), s.copy(cpu = newCpu))
    }
  }
//  def execute: State[Dmg,Unit] = {
//    for {
//      a <- read(A)
//      o <- read(reg)
//      (result, carry, halfCarry) = a sbc o
//      _ <- flags(z = SetIf(result.isZero), n = Setb, h = SetIf(halfCarry), c = SetIf(carry))
//    } yield ()
//  }
}

case object RLCA extends DmgInstruction {
  val length = 1
  val cycles = 4
  val mnemonic = "RLCA"
  def execute: State[Dmg,Unit] = RLC(A).execute
}

case object RLA extends DmgInstruction {
  val length = 1
  val cycles = 4
  val mnemonic = "RLA"
  def execute: State[Dmg,Unit] = RL(A).execute
}

case object RRCA extends DmgInstruction {
  val length = 1
  val cycles = 4
  val mnemonic = "RRCA"
  def execute: State[Dmg,Unit] = RRC(A).execute
}

case object RRA extends DmgInstruction {
  val length = 1
  val cycles = 4
  val mnemonic = "RRA"
  def execute: State[Dmg,Unit] = RR(A).execute
}

//BIT INSTRUCTIONS
case class RLC (reg: DmgOperand[UByte]) extends DmgInstruction {
  val length = 2
  val cycles = if (reg == Ind8(HL)) 16 else 8
  val mnemonic = "RLC"
  def execute: State[Dmg,Unit] = for {
    a0 <- read(reg)
    a1 = if (a0.test(7)) (a0 << 1).set(0) else (a0 << 1).reset(0)
    _ <- write(reg, a1)
    _ <- flags(z = SetIf(a1.isZero), n = Reset, h = Reset, c = SetIf(a0.test(7)))
  } yield ()
}
case class RRC (reg: DmgOperand[UByte]) extends DmgInstruction {
  val length = 2
  val cycles = if (reg == Ind8(HL)) 16 else 8
  val mnemonic = "RRC"
  def execute: State[Dmg,Unit] = for {
    a0 <- read(reg)
    a1 = if (a0.test(0)) (a0 >> 1).set(7) else (a0 >> 1).reset(7)
    _ <- write(reg, a1)
    _ <- flags(z = SetIf(a1.isZero), n = Reset, h = Reset, c = SetIf(a0.test(0)))
  } yield ()
}
case class RL  (reg: DmgOperand[UByte]) extends DmgInstruction {
  val length = 2
  val cycles = if (reg == Ind8(HL)) 16 else 8
  val mnemonic = "RL"
  def execute: State[Dmg,Unit] = for {
    c <- getFlags.map(_.C)
    a0 <- read(reg)
    a1 = if (c) (a0 << 1).set(0) else (a0 << 1).reset(0)
    _ <- write(reg, a1)
    _ <- flags(z = SetIf(a1.isZero), n = Reset, h = Reset, c = SetIf(a0.test(7)))
  } yield ()
}
case class RR  (reg: DmgOperand[UByte]) extends DmgInstruction {
  val length = 2
  val cycles = if (reg == Ind8(HL)) 16 else 8
  val mnemonic = "RR"
  def execute: State[Dmg,Unit] = for {
    c <- getFlags.map(_.C)
    a0 <- read(reg)
    a1 = if (c) (a0 >> 1).set(7) else (a0 >> 1).reset(7)
    _ <- write(reg, a1)
    _ <- flags(z = SetIf(a1.isZero), n = Reset, h = Reset, c = SetIf(a0.test(0)))
  } yield ()
}
case class SLA (reg: DmgOperand[UByte]) extends DmgInstruction {
  val length = 2
  val cycles = if (reg == Ind8(HL)) 16 else 8
  val mnemonic = "SLA"
  def execute: State[Dmg,Unit] = for {
    a0 <- read(reg)
    a1 = a0 << 1
    _ <- write(reg, a1)
    _ <- flags(z = SetIf(a1.isZero), n = Reset, h = Reset, c = SetIf(a0.test(7)))
  } yield ()
}
case class SRA (reg: DmgOperand[UByte]) extends DmgInstruction {
  val length = 2
  val cycles = if (reg == Ind8(HL)) 16 else 8
  val mnemonic = "SRA"
  def execute: State[Dmg,Unit] = for {
    a0 <- read(reg)
    a1 = if (a0.test(7)) (a0 >> 1).set(7) else (a0 >> 1).reset(7)
    _ <- write(reg, a1)
    _ <- flags(z = SetIf(a1.isZero), n = Reset, h = Reset, c = SetIf(a0.test(0)))
  } yield ()
}
case class SWAP(reg: DmgOperand[UByte]) extends DmgInstruction {
  val length = 2
  val cycles = if (reg == Ind8(HL)) 16 else 8
  val mnemonic = "SWAP"
  def execute: State[Dmg,Unit] = for {
    a0 <- read(reg)
    a1 = (a0 << 4) | ((a0 >> 4) & new UByte(0x0f))
    _ <- write(reg, a1)
    _ <- flags(z = SetIf(a1.isZero), n = Reset, h = Reset, c = Reset)
  } yield ()
}
case class SRL (reg: DmgOperand[UByte]) extends DmgInstruction {
  val length = 2
  val cycles = if (reg == Ind8(HL)) 16 else 8
  val mnemonic = "SRL"
  def execute: State[Dmg,Unit] = for {
    a0 <- read(reg)
    a1 = (a0 >> 1).reset(7)
    _ <- write(reg, a1)
    _ <- flags(z = SetIf(a1.isZero), n = Reset, h = Reset, c = SetIf(a0.test(0)))
  } yield ()
}

case class BIT (bit: Int, reg: DmgOperand[UByte]) extends DmgInstruction {
  val length = 2
  val cycles = if (reg == Ind8(HL)) 16 else 8
  val mnemonic = "BIT"
  val execute: State[Dmg,Unit] = new State[Dmg,Unit] {
    override def run(s: Dmg): (Unit, Dmg) = {
      val x = s.peek(reg).test(bit)
      val newCpu = s.cpu.zero(!x).subtract(false).halfCarry(true) //carry not affected
      ((), s.copy(cpu = newCpu))
    }
  }
}

case class RES (bit: Int, reg: DmgOperand[UByte]) extends DmgInstruction {
  val length = 2
  val cycles = if (reg == Ind8(HL)) 16 else 8
  val mnemonic = "RES"
  def execute: State[Dmg,Unit] = modify(reg, _.reset(bit)).map(_ => Unit)
}
case class SET (bit: Int, reg: DmgOperand[UByte]) extends DmgInstruction {
  val length = 2
  val cycles = if (reg == Ind8(HL)) 16 else 8
  val mnemonic = "SET"
  def execute: State[Dmg,Unit] = modify(reg, _.set(bit)).map(_ => Unit)
}

//MISC INSTRUCTIONS
case object DAA extends DmgInstruction { //TODO! decimal adjust when subtracting doesn't work
  val length = 1
  val cycles = 4
  val mnemonic = "DAA"
  def execute: State[Dmg,Unit] = new State[Dmg,Unit] {
    override def run(s: Dmg): (Unit, Dmg) = {
      val a1 = s.cpu.a
      if (!s.cpu.subtract) { //in case it was an addition...
        //If the lower nybble is greater than 9 or the H flag is set, then $06 is added to the register.
        //Then, if the higher nybble is greater than 9 or the C flag is set, then $60 is added.
        //If the second addition was needed, the C flag is set after execution, otherwise it is reset.
        val a2 = if (a1.lowNybble > 9 || s.cpu.halfCarry) a1 + 0x06 else a1
        val (a3, newCarry) = if (a2.highNybble > 9 || s.cpu.carry) (a2 + 0x60, true) else (a2, false)
        val newCpu = s.cpu.a(a3).carry(newCarry).zero(a3.isZero).halfCarry(false)
        ((), s.copy(cpu = newCpu))
      } else { //in case it was a subtraction...
        //read the Z80 documentation in order to understand why the below numbers are added...
        val a2 = if (s.cpu.carry && s.cpu.halfCarry) a1 + 0x9a
        else if (s.cpu.carry) a1 + 0xa0
        else if (s.cpu.halfCarry) a1 + 0xfa
        else a1
        ((), s.copy(cpu = s.cpu.a(a2)))
      }
    }
  }
}

case object CPL extends DmgInstruction {
  val length = 1
  val cycles = 4
  val mnemonic = "CPL"
  def execute: State[Dmg,Unit] = for {
    a <- modify(A, (b: UByte) => ~b)
    _ <- flags(z = NotAffected, n = Setb, h = Setb, c = NotAffected)
  } yield ()
}
case object CCF extends DmgInstruction {
  val length = 1
  val cycles = 4
  val mnemonic = "CCF"
  def execute: State[Dmg,Unit] = for {
    f <- getFlags
    _ <- flags(z = NotAffected, n = Reset, h = Reset, c = SetIf(!f.C))
  } yield ()
}
case object SCF extends DmgInstruction {
  val length = 1
  val cycles = 4
  val mnemonic = "SCF"
  def execute: State[Dmg,Unit] = for {
    _ <- flags(z = NotAffected, n = Reset, h = Reset, c = Setb)
  } yield ()
}

case object HALT extends DmgInstruction {
  val length = 1
  val cycles = 4
  val mnemonic = "HALT"
  def execute = new State[Dmg, Unit] {
    override def run(s: Dmg): (Unit, Dmg) = ((), s.copy(cpu = s.cpu.halt))
  }
}

case class RST(addr: UByte) extends DmgInstruction {
  val length = 1
  val cycles = 32
  val mnemonic = "RST"
  def execute: State[Dmg,Unit] = CALL(Unconditional, Imm16(new UByte(0x00) hl addr)).execute
}

case class JP(cond: Condition, addr: DmgOperand[U16]) extends DmgInstruction {
  val length = 1 + operandLength(addr)
  val cycles = if (addr == Ind16(HL)) 4 else 12 //TODO dodgy way of resolving this
  val mnemonic = "JP"
  def execute: State[Dmg,Unit] = for {
    doIt <- check(cond)
    _ <- if (doIt) Dmg.copy16(PC, addr) else doNothing
  } yield ()
}
case class JR(cond: Condition, relAddr: Int) extends DmgInstruction {
  val length = 2
  val cycles = 8
  val mnemonic = "JR"
  override def toString = mnemonic + " " + cond + ", " + relAddr
  def execute: State[Dmg,Unit] = new State[Dmg,Unit] {
    override def run(s: Dmg): (Unit, Dmg) = {
      val doIt = cond.check(s.cpu)
      val newCpu = if (doIt) s.cpu.pc(new U16(s.cpu.pc.value + relAddr)) else s.cpu
      ((), s.copy(cpu = newCpu))
    }
  }
}
case class CALL(cond: Condition, addr: DmgOperand[U16]) extends DmgInstruction {
  val length = 3 //can only be immediate value
  val cycles = 12
  val mnemonic = "CALL"
  override def toString = mnemonic + " " + (if (cond == Unconditional) "" else cond + ", ") + addr
  def execute: State[Dmg,Unit] = for {
    doIt <- check(cond)
    _ <- if (doIt) PUSH(PC).execute else doNothing
    _ <- if (doIt) Dmg.copy16(PC, addr) else doNothing
  } yield ()
}
case class RET(cond: Condition) extends DmgInstruction {
  val length = 1
  val cycles = 8
  val mnemonic = "RET"
  val execute: State[Dmg,Unit] = new State[Dmg,Unit] {
    override def run(s: Dmg): (Unit, Dmg) = {
      if (cond.check(s.cpu)) POP(PC).execute.run(s) else ((),s)
    }
  }
}
case object RETI extends DmgInstruction {
  val length = 1
  val cycles = 8
  val mnemonic = "RETI"
  def execute: State[Dmg,Unit] = RET(Unconditional).execute andThen EI.execute
}

case object DI extends DmgInstruction {
  val length = 1
  val cycles = 4
  val mnemonic = "DI"
  def execute: State[Dmg,Unit] = new State[Dmg,Unit] {
    override def run(s: Dmg): (Unit, Dmg) = ((), s.copy(cpu = s.cpu.copy(ime = false))) //TODO use lens!
  }
}

case object EI extends DmgInstruction {
  val length = 1
  val cycles = 4
  val mnemonic = "EI"
  def execute: State[Dmg,Unit] = new State[Dmg,Unit] {
    override def run(s: Dmg): (Unit, Dmg) = ((), s.copy(cpu = s.cpu.copy(ime = true))) //TODO use lens!
  }
}

case class LDHL_SP_Imm(immValue: Int) extends DmgInstruction {
  val length = 2
  val cycles = 12
  val mnemonic = "LD HL, SP + ("+immValue+")"
  def execute: State[Dmg,Unit] = new State[Dmg,Unit] {
    override def run(s: Dmg): (Unit, Dmg) = {
      val valueToLoad: U16 = s.peek[U16](Ind16(Imm16(new U16(s.cpu.sp.value + immValue))))
      //TODO carry and halfcarry behaviour? no idea
      val newCpu = s.cpu.hl(valueToLoad).zero(false).subtract(false)
      ((), s.copy(cpu = newCpu))
    }
  }
}

case class LD_IndImm_SP(imm16: U16) extends DmgInstruction {
  val length = 3
  val cycles = 20
  val mnemonic = "LD (" + imm16 + "), SP"
  def execute: State[Dmg,Unit] = new State[Dmg,Unit] {
    override def run(s: Dmg): (Unit, Dmg) = {
      val newS = s.poke(Ind16(Imm16(imm16)), s.cpu.sp)
      ((), newS)
    }
  }
}

abstract case class STOP() extends DmgInstruction