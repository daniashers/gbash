package com.greasybubbles.gbash.parsers

import com.greasybubbles.gbash._
import com.greasybubbles.gbash.cpu._

import scala.annotation.tailrec

object DmgAllInstructionParsers {

  val parsers: List[DmgInstructionParser] = List(

    LdParser, //parses all loads to and from CPU registers and (HL) (pattern 01DDDSSS) except for 01110110)
    ImmediateLdParser, //parses all load of immiediate values (pattern 00DDD110 0x??)
    ArithmeticParser, //parses all 8-bit arithmetic (pattern 10ooorrr)
    ImmediateArithmeticParser, //parses immediate 8-bit arithmetic (pattern 11ooo110)
    AbsoluteJumpParser,
    RelativeJumpParser,
    CallParser,
    ReturnParser,
    All16bitOpsParser, //parses all 16-bit loads (pattern 00rrx0y1 and others)
    RestartParser, //parses all restarts (pattern 11aaa111)
    IncDecParser, //parses all 8-bit inc and decs (pattern 00rrr10x)
    PushPopParser, //parses all stack push/pop ops (pattern 11rr0x01)
    LdhParser, //parses all page FF loads pattern (111.00.0)
    BitInstructionParser, //parses all bit instructions (two bytes, pattern 0xCB 0x??)

    //TODO group these into their own parser? Is it worth it?
    DmgInstructionParser.generic(0x0A, LD(A, Ind8(BC))),
    DmgInstructionParser.generic(0x1A, LD(A, Ind8(DE))),
    DmgInstructionParser.generic(0x02, LD(Ind8(BC), A)),
    DmgInstructionParser.generic(0x12, LD(Ind8(DE), A)),

    //loads with increment/decrement
    DmgInstructionParser.generic(0x3A, LDD(A, Ind8(HL))), //dec HL after load
    DmgInstructionParser.generic(0x32, LDD(Ind8(HL), A)), //dec HL after load
    DmgInstructionParser.generic(0x2A, LDI(A, Ind8(HL))), //inc HL after load
    DmgInstructionParser.generic(0x22, LDI(Ind8(HL), A)), //inc HL after load

    // Misc
    DmgInstructionParser.generic(0x3F, CCF), // Complement carry flag
    DmgInstructionParser.generic(0x37, SCF),
    DmgInstructionParser.generic(0xF3, DI),
    DmgInstructionParser.generic(0xFB, EI),
    DmgInstructionParser.generic(0x00, NOP),
    DmgInstructionParser.generic(0x2F, CPL),
    DmgInstructionParser.generic(0x76, HALT),

    // Rotates and shifts
    DmgInstructionParser.generic(0x07, RLCA),
    DmgInstructionParser.generic(0x17, RLA),
    DmgInstructionParser.generic(0x0F, RRCA),
    DmgInstructionParser.generic(0x1F, RRA),

    DmgInstructionParser.generic(0x27, DAA)

    //DmgInstructionParser.genericDouble(0x10, 0x00, STOP())

  )

  val anyParser: DmgInstructionParser = new DmgInstructionParser {
    def parse(s: Stream[UByte]): Option[DmgInstruction] = {
      @tailrec
      def go(parsersLeftToTry: List[DmgInstructionParser]): Option[DmgInstruction] = parsersLeftToTry match {
        case Nil => None
        case h :: t => {
          val result = parsersLeftToTry.head.parse(s)
          if (result != None) result else go(t)
        }
      }
      go(parsers)
    }
  }
}

object PrecalcParser {

  trait PrecalculatedInstruction
  case class ActualInstruction(i: DmgInstruction) extends PrecalculatedInstruction
  case class Length2Instruction(leaves: IndexedSeq[DmgInstruction]) extends PrecalculatedInstruction
  case class Length3Instruction(leaves: IndexedSeq[DmgInstruction]) extends PrecalculatedInstruction

  def findParser(s: Stream[UByte]): Option[DmgInstructionParser] = {
    @tailrec
    def go(parsersLeftToTry: List[DmgInstructionParser]): Option[DmgInstructionParser] = parsersLeftToTry match {
      case Nil => None
      case h::t => {
        val result = parsersLeftToTry.head.parse(s)
        if (result != None) Some(parsersLeftToTry.head) else go(t)
      }
    }
    go(DmgAllInstructionParsers.parsers)
  }

  lazy val precalcInstrTable: Array[PrecalculatedInstruction] = Array.tabulate(256)(i => {
    println("Precalculating %d".format(i))
    val stream = new UByte(i) #:: UByte.ZERO #:: UByte.ZERO #:: Stream.empty
    val instr = DmgAllInstructionParsers.anyParser.parse(stream)
    if (instr.getOrElse(NOP).length == 1) ActualInstruction(instr.getOrElse(NOP))
    else if (instr.getOrElse(NOP).length == 2) {
      val parser = findParser(stream).get
      val subInstructions = Array.tabulate[DmgInstruction](256)(j => {
        parser.parse(new UByte(i) #:: new UByte(j) #:: UByte.ZERO #:: Stream.empty).getOrElse(NOP)
      })
      Length2Instruction(subInstructions)
    }
    else if (instr.getOrElse(NOP).length == 3) {
      val parser = findParser(stream).get
      val subInstructions = Array.tabulate[DmgInstruction](65536)(k => {
        parser.parse(new UByte(i) #:: new U16(k).lo #:: new U16(k).hi #:: Stream.empty).getOrElse(NOP)
      })
      Length3Instruction(subInstructions)
    }
    else ActualInstruction(NOP)
  })

  lazy val precalcParserTable: Array[Option[DmgInstructionParser]] = Array.tabulate(256)(i => {
    findParser(new UByte(i) #:: UByte.ZERO #:: UByte.ZERO #:: Stream.empty)
  })

  def parse(s: Stream[UByte]): Option[DmgInstruction] = {
    precalcInstrTable(s.head.value) match {
      case ActualInstruction(i) => Some(i)
      case Length2Instruction(leaves) => Some(leaves(s.drop(1).head.value))
      case Length3Instruction(leaves) => Some(leaves((s.drop(2).head hl s.drop(1).head).value))
      case _ => None
    }
  }

  def parze(b0: UByte, b1: => UByte, b2: => UByte): Option[DmgInstruction] = {
    precalcInstrTable(b0.value) match {
      case ActualInstruction(i) => Some(i)
      case Length2Instruction(leaves) => Some(leaves(b1.value))
      case Length3Instruction(leaves) => Some(leaves((b2 hl b1).value))
      case _ => None
    }
  }

}

object TableBasedParser /* extends DmgInstructionParser */ {

//  def parse(s: Stream[UByte]): Option[DmgInstruction] = {
//    def argByte = s.drop(1).head
//    def argWord = s.drop(2).head hl s.drop(1).head
//
//    Opcode.ops(s.head.value) match {
//      case NoArgInstruction(i) => Some(i)
//      case ByteArgInstruction(f8) => Some(f8(argByte))
//      case WordArgInstruction(f16) => Some(f16(argWord))
//      case _ => None
//    }
//  }
//

  def parse(b0: UByte, b1: => UByte, b2: => UByte): Option[DmgInstruction] = {
    def argByte = b1
    def argWord = b2 hl b1
    val opc = Opcode.ops(b0.value)
    opc match {
      case NoArgInstruction(i) => Some(i)
      case ByteArgInstruction(f8) => Some(f8(argByte))
      case WordArgInstruction(f16) => Some(f16(argWord))
      case _ => None
    }
  }
}

object CachingInstr {
  val CacheEntriesNr = 5
  var cacheAddresses = Vector.tabulate[Int](CacheEntriesNr)(_ => -1)
  var cacheInstr = Vector.tabulate[DmgInstruction](CacheEntriesNr)(_ => null)
  var cachePtr: Int = 0
  def checkCache(addr: Int): DmgInstruction = {
    var i = 0
    var found: DmgInstruction = null
    while (i < CacheEntriesNr) {
      if (cacheAddresses(i) == addr) found = cacheInstr(i)
      i = i+1
    }
    //if (found==null) print(" Cache miss ")
    found
  }

  def feedCache(addr: Int, instr: DmgInstruction): Unit = {
    cacheAddresses = cacheAddresses.updated(cachePtr, addr)
    cacheInstr = cacheInstr.updated(cachePtr, instr)
    cachePtr = (cachePtr + 1) % CacheEntriesNr
  }
}