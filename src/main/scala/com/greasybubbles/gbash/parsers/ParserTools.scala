package com.greasybubbles.gbash.parsers

import com.greasybubbles.gbash.{U16, UByte, DmgInstruction}

trait Parser[A,B] {
  def parse(s: Stream[A]): Option[B]
}

object Parser {
  def unit[A,B](value: B): Parser[A,B] = new Parser[A,B] {
    def parse(s: Stream[A]): Option[B] = Some(value)
  }
  def combine[A, B, C, D](p1: Parser[A,B], p2: Parser[A, C])(f: (B,C) => D): Parser[A,D] = new Parser[A,D] {
    def parse(s: Stream[A]): Option[D] = {
      val v1: Option[B] = p1.parse(s)
      val v2: Option[C] = p2.parse(s)
      //TODO implement
      null
    }
  }
}

object ByteParser extends Parser[UByte,UByte] {
  def parse(s: Stream[UByte]): Option[UByte] = Some(s.head)
}

object WordParser extends Parser[UByte,U16] {
  def parse(s: Stream[UByte]): Option[U16] = Some(s.drop(1).head hl s.head)
}

class PartialParser[A](f: A => DmgInstruction) {
  //def parseMore(s: Stream[UByte]): A
    def parse(arg: A): DmgInstruction = f(arg)
}
