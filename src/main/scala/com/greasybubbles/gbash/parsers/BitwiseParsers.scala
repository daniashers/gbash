package com.greasybubbles.gbash.parsers

import com.greasybubbles.gbash._
import com.greasybubbles.gbash.cpu._

import BitwiseParsers._

import scala.annotation.tailrec

trait DmgInstructionParser extends Parser[UByte,DmgInstruction]

object DmgInstructionParser {

  def generic(opcode: UByte, instr: DmgInstruction): DmgInstructionParser = new DmgInstructionParser {
    def parse(s: Stream[UByte]): Option[DmgInstruction] = if (s.head.value == opcode.value) Some(instr) else None
  }

  def genericDouble(opcode1: UByte, opcode2: UByte, instr: DmgInstruction): DmgInstructionParser = new DmgInstructionParser {
    def parse(s: Stream[UByte]): Option[DmgInstruction] = {
      if (s.head == opcode1 && s.drop(1).head == opcode2) Some(instr) else None
    }
  }
}

object BitwiseParsers {
  def argByte(s: Stream[UByte]) = s.drop(1).head
  def argWord(s: Stream[UByte]) = s.drop(2).head hl s.drop(1).head

  def code2Reg(code: Int): DmgOperand[UByte] = code match {
    case 0 => B
    case 1 => C
    case 2 => D
    case 3 => E
    case 4 => H
    case 5 => L
    case 6 => Ind8(HL)
    case 7 => A
  }

  def code2Reg16withAF(code: Int): DmgRegister16 = code match {
    case 0 => BC
    case 1 => DE
    case 2 => HL
    case 3 => AF
  }

  def code2Reg16withSP(code: Int): DmgRegister16 = code match {
    case 0 => BC
    case 1 => DE
    case 2 => HL
    case 3 => SP
  }

  def code2Condition(code: Int): Condition = code match {
    case 0 => NZ
    case 1 => Z
    case 2 => NC
    case 3 => CS
  }

  //this only works for patterns exactly 8 bits long.
  def doesMatch(value: Int, ptn: String): Boolean = {
    @tailrec
    def ptnMatch(value: Int, expected: String): Boolean = {
      if (expected.length == 0) true
      else {
        val expectedLsbChar = expected.charAt(expected.length-1)
        if (expectedLsbChar == '0' || expectedLsbChar == '1') {
          val expectedLsb = expectedLsbChar.toInt
          if (expectedLsbChar != (value & 0x01)) false
          else ptnMatch(value >> 1, expected.substring(0,expected.length-2))
        } else ptnMatch(value >> 1, expected.substring(0,expected.length-2))
      }
    }
    if (ptn.length != 8) false
    else ptnMatch(value, ptn)
  }

  //usage: binMatch(value, "01xxx0yy") will return (xxx,yy) IF the fixed bits match, otherwise nothing.
  def binMatch(value: Int, ptn: String): Option[List[Int]] = {
    val signatureBitsMask = Integer.parseInt(ptn.replaceAll("[01]","1").replaceAll("[xyz]", "0"), 2)
    val signatureBitsWithVariableBitsZeroed = Integer.parseInt(ptn.replaceAll("[xyz]", "0"), 2)
    val matchesAtAll: Boolean = ((value & signatureBitsMask) ^ signatureBitsWithVariableBitsZeroed) == 0
    if (!matchesAtAll) None else {
      val extractedValues = for {
        x <- List('x', 'y', 'z')
        relevantBitsOnly = value & Integer.parseInt(ptn.replaceAll("[^" + x + "]", "0").replaceAll(x.toString, "1"), 2);
        lastIndex = ptn.lastIndexOf(x)
      } yield (relevantBitsOnly >> (7 - lastIndex))
      Some(extractedValues)
    }
  }
  def binMatch1(value: Int, ptn: String): Option[Int] = binMatch(value: Int, ptn).map(_.head)
  def binMatch2(value: Int, ptn: String): Option[(Int,Int)] = binMatch(value: Int, ptn).map(result => (result(0), result(1)))
  def binMatch3(value: Int, ptn: String): Option[(Int,Int,Int)] = binMatch(value: Int, ptn).map(result => (result(0), result(1), result(2)))

}

case object LdParser extends DmgInstructionParser {
  def parse(s: Stream[UByte]) = {
    val v = s.head.value
    if (v == 0x76) None //0x01110110
    else if (v == 0xFA) Some(LD(A, Ind8(Imm16(argWord(s)))))
    else if (v == 0xEA) Some(LD(Ind8(Imm16(argWord(s))), A))
    else binMatch2(v, "01xxxyyy").map(result => {
      val (destination, source) = result
      LD(code2Reg(destination), code2Reg(source))
    })
  }
}

case object ImmediateLdParser extends DmgInstructionParser {
  def parse(s: Stream[UByte]) = {
    val opcode = s.head.value
    binMatch1(opcode, "00xxx110").map(result => {
      def destination = code2Reg(result)
      LD(destination, Imm8(argByte(s)))
    })
  }
}

case object ArithmeticParser extends DmgInstructionParser {
  def parse(s: Stream[UByte]) = {
    binMatch2(s.head.value, "10xxxyyy").map(result => {
      val (operationCode, registerCode) = result
      val register = code2Reg(registerCode)
      operationCode match {
        case 0 => ADD(register)
        case 1 => ADC(register)
        case 2 => SUB(register)
        case 3 => SBC(register)
        case 4 => AND(register)
        case 5 => XOR(register)
        case 6 => OR (register)
        case 7 => CP (register)
      }
    })
  }
}

case object ImmediateArithmeticParser extends DmgInstructionParser {
  def parse(s: Stream[UByte]) = {
    lazy val arg = s.drop(1).head
    binMatch1(s.head.value, "11xxx110").map(result => {
      val operationCode = result
      operationCode match {
        case 0 => ADD(Imm8(arg))
        case 1 => ADC(Imm8(arg))
        case 2 => SUB(Imm8(arg))
        case 3 => SBC(Imm8(arg))
        case 4 => AND(Imm8(arg))
        case 5 => XOR(Imm8(arg))
        case 6 => OR (Imm8(arg))
        case 7 => CP (Imm8(arg))
      }
    })
  }
}

case object LdhParser extends DmgInstructionParser {
  def parse(s: Stream[UByte]) = {
    lazy val arg = s.drop(1).head
    binMatch2(s.head.value, "111x00y0").map(_ match {
      case (1,1) => LDH(A,IndirectHi(C))
      case (0,1) => LDH(IndirectHi(C),A)
      case (1,0) => LDH(A,IndirectHi(Imm8(arg)))
      case (0,0) => LDH(IndirectHi(Imm8(arg)),A)
    })
  }
}

case object BitInstructionParser extends DmgInstructionParser {
  def parse(s: Stream[UByte]) = {
    if (s.head.value != 0xCB) None
    else if (s.tail == Stream.empty) None //it is 2 bytes long so if there are no more bytes it can't be a bit instr
    else {
      val (arg1, arg2, registerCode) = binMatch3(s.drop(1).head.value, "xxyyyzzz").get
      val register = code2Reg(registerCode)
      val instr: DmgInstruction = arg1 match {
        case 0 => arg2 match {
          case 0 => RLC (register)
          case 1 => RRC (register)
          case 2 => RL  (register)
          case 3 => RR  (register)
          case 4 => SLA (register)
          case 5 => SRA (register)
          case 6 => SWAP(register)
          case 7 => SRL (register)
        }
        case 1 => BIT(arg2,register)
        case 2 => RES(arg2,register)
        case 3 => SET(arg2,register)
      }
      Some(instr)
    }
  }
}

case object RestartParser extends DmgInstructionParser {
  def parse(s: Stream[UByte]) = {
    binMatch1(s.head.value, "11xxx111").map(_ match {
      case 0 => RST(0x00)
      case 1 => RST(0x08)
      case 2 => RST(0x10)
      case 3 => RST(0x18)
      case 4 => RST(0x20)
      case 5 => RST(0x28)
      case 6 => RST(0x30)
      case 7 => RST(0x38)
    })
  }
}

case object AbsoluteJumpParser extends DmgInstructionParser {
  import com.greasybubbles.gbash.parsers.DmgInstructionParser._
  def parse(s: Stream[UByte]) = {
    val byte1 = s.head
    lazy val addr = s.drop(2).head hl s.drop(1).head
    if (byte1.value == 0xc3) Some(JP(Unconditional, Imm16(addr)))
    else if (byte1.value == 0xe9) Some(JP(Unconditional, HL))
    else binMatch1(byte1.value, "110xx010").map(x => JP(code2Condition(x), Imm16(addr)))
  }
}

case object RelativeJumpParser extends DmgInstructionParser {
  import com.greasybubbles.gbash.parsers.DmgInstructionParser._
  def parse(s: Stream[UByte]) = {
    val byte1 = s.head
    lazy val relativeAddr = s.drop(1).head
    if (byte1.value == 0x18) Some(JR(Unconditional, relativeAddr.signedValue))
    else binMatch1(byte1.value, "001xx000").map(x => JR(code2Condition(x), relativeAddr.signedValue))
  }
}

case object CallParser extends DmgInstructionParser {
  import com.greasybubbles.gbash.parsers.DmgInstructionParser._
  def parse(s: Stream[UByte]) = {
    val byte1 = s.head
    lazy val addr = s.drop(2).head hl s.drop(1).head
    if (byte1.value == 0xCD) Some(CALL(Unconditional, Imm16(addr)))
    else binMatch1(byte1.value, "110xx100").map(x => CALL(code2Condition(x), Imm16(addr)))
  }
}

case object ReturnParser extends DmgInstructionParser {
  import com.greasybubbles.gbash.parsers.DmgInstructionParser._
  def parse(s: Stream[UByte]) = {
    val byte1 = s.head
    lazy val addr = s.drop(2).head hl s.drop(1).head
    if (byte1.value == Integer.parseInt("11011001", 2)) Some(RETI)
    else if (byte1.value == Integer.parseInt("11001001", 2)) Some(RET(Unconditional)) //TODO how to express this HL
    else binMatch1(byte1.value, "110xx000").map(x => RET(code2Condition(x)))
  }
}

case object IncDecParser extends DmgInstructionParser {
  import com.greasybubbles.gbash.parsers.DmgInstructionParser._
  def parse(s: Stream[UByte]) = {
    binMatch2(s.head.value, "00xxx10y").map(result => {
      val (registerCode, mode) = result
      val register = code2Reg(registerCode)
      if (mode == 0) INC(register)
      else DEC(register)
    })
  }
}

case object PushPopParser extends DmgInstructionParser {
  import com.greasybubbles.gbash.parsers.DmgInstructionParser._
  def parse(s: Stream[UByte]) = {
    binMatch2(s.head.value, "11xx0y01").map(result => {
      val (registerCode, mode) = result
      val register = code2Reg16withAF(registerCode)
      if (mode == 0) POP(register)
      else PUSH(register)
    })
  }
}

case object All16bitOpsParser extends DmgInstructionParser {
  import com.greasybubbles.gbash.parsers.DmgInstructionParser._
  def parse(s: Stream[UByte]) = {
    val byte1 = s.head
    lazy val value = s.drop(2).head hl s.drop(1).head

    if (byte1.value == 0xF9) Some(LD16(SP, HL))
    else if (byte1.value == 0xE8) Some(ADDSP(s.drop(1).head.signedValue))
    else if (byte1.value == 0xF8) Some(LDHL_SP_Imm(s.drop(1).head.signedValue)) //TODO refactor, it's ugly
    else if (byte1.value == 0x08) Some(LD_IndImm_SP(value))
    else binMatch3(s.head.value, "00xxy0z1").map(result => {
      val (registerCode,op1,op2) = result
      val register = code2Reg16withSP(registerCode)
      (op1,op2) match {
        case (0,0) => LD16(register, Imm16(value))
        case (1,0) => ADDHL(register)
        case (0,1) => INC16(register)
        case (1,1) => DEC16(register)
      }
    })
  }
}
