package com.greasybubbles.gbash

sealed trait InterruptType

abstract class DmgInterruptType(val priority: Int, val address: U16, val flagBitNr: Int)

case object VBlankInterrupt         extends DmgInterruptType(1, 0x0040, 0)
case object LCDCStatusInterrupt     extends DmgInterruptType(2, 0x0048, 1)
case object TimerOverflowInterrupt  extends DmgInterruptType(3, 0x0050, 2)
case object SerialTransferInterrupt extends DmgInterruptType(4, 0x0058, 3)
case object ButtonInterrupt         extends DmgInterruptType(5, 0x0060, 4)
