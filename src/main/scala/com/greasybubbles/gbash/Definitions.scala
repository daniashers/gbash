package com.greasybubbles.gbash

trait State[S,A] { self =>
  def run(s: S): (A,S) //abstract
  def map[B](f: A => B) = new State[S,B] {
    def run(s: S) = {
      val (a, s2) = self.run(s)
      (f(a), s2)
    }
  }
  def flatMap[B](g: A => State[S,B]) = new State[S,B] {
    def run(s: S) = {
      val (a, s2) = self.run(s)
      g(a).run(s2)
    }
  }
  def andThen[B](next: State[S,B]) = flatMap(_ => next)
}

object State {
  def apply[S,A](f: S => (A,S)) = new State[S,A] {
    def run(s: S): (A,S) = f(s)
  }

  implicit def functionToTransition[S,A](f: S => (A,S)): State[S,A] = State(f)
}

//----------------------------

trait UnitState[S] { self =>
  def run(s: S): S //abstract
  def andThen(next: UnitState[S]) = new UnitState[S] {
    def run(s: S): S = next.run(this.run(s))
  }
}

object UnitState {
  def apply[S](f: S => S) = new UnitState[S] {
    def run(s: S): S = f(s)
  }

  implicit def functionToTransition[S](f: S => S): UnitState[S] = UnitState(f)
}

trait Instruction[System] {
  def mnemonic: String
  def execute: State[System,Unit]
}

trait CpuInstruction[System] extends Instruction[System] {
  def length: Int
  def cycles: Int
}

//---

trait Operand[S,A]

